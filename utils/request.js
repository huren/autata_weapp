import Toast from '@vant/weapp/toast/toast';

const url = require('./url.js').default 
const util = require("./util.js");
const app = getApp();

const params = function (json) {
  return Object.keys(json).map(function (key) {
    return encodeURIComponent(key) + "=" + encodeURIComponent(json[key]);
  }).join("&");
} 

const globalData = {
  exeQueue: true,
  needBeginLogin: true,
  promiseQueue: []
}

const navigateToLogin = function (tips) {
  Toast.clear();

  // 清空token
  app.saveToken(null);
  app.saveRefreshToken(null);

  let pages = getCurrentPages();
  let beforePage = pages[pages.length - 2]; 
  beforePage && beforePage.refreshWebUserInfo && beforePage.refreshWebUserInfo();
  beforePage && beforePage.refreshData && beforePage.refreshData();

  if (util.getCurrentPageUrl().indexOf('/user/login/index') < 0) {
    wx.navigateTo({
      url: '/pages/user/login/index',
      success: function(res) {
        setTimeout(function () {
          if (tips) {
            Toast.fail(tips);
          }
        }, 1000) 
      }
    })
  } 
  // else {
  //   if (tips) {
  //     Toast.fail(tips);
  //   }
  // }
}

const getNewToken = function (callback) {
  app.wxLogin(() => {
    let params = {
      code: app.getWxLoginCode()
    };
    wxLogin(params).then(res => {
      if (res && res.statusCode == 200) {
        console.log('web平台登录成功！');
        // 存储token
        app.saveToken(res.data.token);
        app.saveRefreshToken(res.data.refreshToken);
        // 重新登录成功
        callback && callback();
      } else {
        console.log('web平台用户登录失败！' + res.errMsg);
        navigateToLogin('您的登录过期');
      }
    })
  }, () => {
    navigateToLogin('您的登录过期');
  })
}

/**
 * 封装的promise
 * 参数： requestObj 请求成功回调
 */
const promiseRequest = function (requestObj) {
  let token = getApp().getToken();
  return new Promise((resolve, reject) => {
    //网络请求
    wx.request({
      url: requestObj.url,
      method: requestObj.method,
      data: requestObj.data ? JSON.stringify(requestObj.data) : null,
      header: {
        'X-Authorization': token ? 'Bearer ' + token : null,
        'content-type': requestObj.contentType ? requestObj.contentType : 'application/json'
      },
      success: function (res) { //返回取得的数据
        console.log(res);
        let promiseQueue = globalData.promiseQueue;   
        if(res.statusCode == 403){
          Toast.fail("没有权限");
        }else if (res.statusCode == 401) { // token失效，需获取token
          if (res.data) {
            if (res.data.errorCode == 10) {
              // 10 未绑定平台
              if (requestObj.url.indexOf(url.apiUrl.wxLogin) >= 0) {
                navigateToLogin('请先绑定平台');
              } else {
                resolve(res);
              }
            } else if (res.data.errorCode == 11) {
              // 11 用户用wxcode重新登录
              requestObj.resolve = resolve;
              promiseQueue.push(requestObj); //请求失败了，把该请求放到promise队列，等待更新token后重新调用。
              if (!globalData.needBeginLogin) { //如果不需要重新登录
                return;
              }
              // 防止重复调用login。
              globalData.needBeginLogin = false;
              getNewToken(() => { //获取完token以后执行回调
                //重新登陆以后调用一次队列中的promise；并设置队列为可循环状态。
                let promiseQueueItem = promiseQueue.shift();
                if (promiseQueueItem) {
                  globalData.exeQueue = true;
                  promiseRequest(promiseQueueItem);
                  globalData.promiseQueue = promiseQueue;
                }
              }, true)
            }
          } else {
            console.error("接口返回数据格式错误"); 
          }
        } else {
          if (requestObj.resolve) { //如果是promise队列中的请求。
            requestObj.resolve(res);
            let promiseQueueItem = promiseQueue.shift();
            if (globalData.exeQueue) { //如果队列可执行则循环队列，保持队列只被循环一次。
              globalData.exeQueue = false; //防止被多次循环。
              while (promiseQueueItem) {
                promiseRequest(promiseQueueItem);
                promiseQueueItem = promiseQueue.shift();
                globalData.promiseQueue = promiseQueue;
              }
              if (!promiseQueueItem) {
                globalData.exeQueue = true;
                globalData.needBeginLogin = true;
              }
            }
          } else {
            resolve(res);
          }
        }
      },
      fail: function (res) {
        // fail调用接口失败
        console.error(res);
        reject({
          errMsg: '网络错误,请稍后重试',
          statusCode: -1
        });
      }
    })
  });
}

// 账号登录
let accountLogin = (data) => {
  let requestObj = {
    url: url.apiUrl.accountLogin,
    method: 'post',
    data: data
  };
  return new Promise((resolve, reject) => {
    resolve(promiseRequest(requestObj))
  })
}

// 微信登录
let wxLogin = (data) => {
  let requestObj = {
    url: url.apiUrl.wxLogin,
    method: 'post',
    data: data
  };
  return new Promise((resolve, reject) => {
    resolve(promiseRequest(requestObj))
  })
}

// 微信与平台绑定
let userBindWx = (data) => {
  let requestObj = {
    url: url.apiUrl.userBindWx,
    method: 'post',
    data: data
  };
  return new Promise((resolve, reject) => {
    resolve(promiseRequest(requestObj))
  })
}

// oss token
let getOssToken = (data) => {
  let requestObj = {
    url: url.apiUrl.oss,
    method: 'get'
  };
  return new Promise((resolve, reject) => {
    resolve(promiseRequest(requestObj))
  })
}

// 账号
let user = (userId, data) => {
  let requestObj = {
    url: url.apiUrl.user + '/' + userId,
    method: 'get'
  };
  return new Promise((resolve, reject) => {
    resolve(promiseRequest(requestObj))
  })
}

// 用户所属门店
let userStore = (data) => {
  let requestObj = {
    url: url.apiUrl.relations + '/detail?' + params(data),
    method: 'get'
  };
  return new Promise((resolve, reject) => {
    resolve(promiseRequest(requestObj))
  })
}

// 门店设备列表
let storeDeviceList = (data) => {
  let requestObj = {
    url: url.apiUrl.relations + '/detail?' + params(data),
    method: 'get'
  };
  return new Promise((resolve, reject) => {
    resolve(promiseRequest(requestObj))
  })
}

// 绑定门店设备
let bindStoreDevice = (data) => {
  let requestObj = {
    url: url.apiUrl.relation,
    method: 'post',
    data: data
  };
  return new Promise((resolve, reject) => {
    resolve(promiseRequest(requestObj))
  })
}

// 解除设备绑定
let unBindStoreDevice = (data) => {
  let requestObj = {
    url: url.apiUrl.relation + '?' + params(data),
    method: 'delete'
  };
  return new Promise((resolve, reject) => {
    resolve(promiseRequest(requestObj))
  })
}

// 设备详情
let getDevice = (data) => {
  let requestObj = {
    url: url.apiUrl.device + '/' + data.id,
    method: 'get'
  };
  return new Promise((resolve, reject) => {
    resolve(promiseRequest(requestObj))
  })
}

// 设备详情
let getDeviceByName = (data) => {
  let requestObj = {
    url: url.apiUrl.device + '/name/' + data.id,
    method: 'get'
  };
  return new Promise((resolve, reject) => {
    resolve(promiseRequest(requestObj))
  })
}

// 保存设备
let saveDevice = (data) => {
  let requestObj = {
    url: url.apiUrl.device,
    method: 'post',
    data: data
  };
  return new Promise((resolve, reject) => {
    resolve(promiseRequest(requestObj))
  })
}

// 设备报修
let reportRepair = (customerId, data) => {
  let requestObj = {
    url: url.apiUrl.customer + '/' + customerId + '/deviceRepair',
    method: 'post',
    data: data
  };
  return new Promise((resolve, reject) => {
    resolve(promiseRequest(requestObj))
  })
}

// 模式、配方详情(与平台服务交互)
let getModel = (data) => {
  let requestObj = {
    url: url.apiUrl.asset + '/' + data.id,
    method: 'get'
  };
  return new Promise((resolve, reject) => {
    resolve(promiseRequest(requestObj))
  })
}

// 设备模式设置(与设备交互)
let setDeviceModel = (deviceId, data) => {
  let requestObj = {
    url: url.apiUrl.deviceModel + '/' + deviceId,
    method: 'post',
    data: data
  };
  return new Promise((resolve, reject) => {
    resolve(promiseRequest(requestObj))
  })
}

// 删除更新的模式
let deleteNewModel = (deviceId, data) => {
  let requestObj = {
    url: url.apiUrl.deviceTelemetry + '/' + deviceId + '/SERVER_SCOPE?' + params(data),
    method: 'delete'
  };
  return new Promise((resolve, reject) => {
    resolve(promiseRequest(requestObj))
  })
}

// 获取设备模式(与设备交互)
let getDeviceModel = (deviceId, data) => {
  let requestObj = {
    url: url.apiUrl.deviceModel + '/' + deviceId,
    method: 'post',
    data: data
  };
  return new Promise((resolve, reject) => {
    resolve(promiseRequest(requestObj))
  })
}

// 启动设备模式(与设备交互)
let setDelayDeviceModel = (deviceId, data) => {
  let requestObj = {
    url: url.apiUrl.delayDeviceModel + '/' + deviceId,
    method: 'post',
    data: data
  };
  return new Promise((resolve, reject) => {
    resolve(promiseRequest(requestObj))
  })
}

// 取消预约模式
let deleteDelayDeviceModel = (deviceId, data) => {
  let requestObj = {
    url: url.apiUrl.deleteDelayDeviceModel + '/' + deviceId,
    method: 'post',
    data: data
  };
  return new Promise((resolve, reject) => {
    resolve(promiseRequest(requestObj))
  })
}

// 预热设置(与设备交互)
let setDeviceHot = (deviceId, data) => {
  let requestObj = {
    url: url.apiUrl.deviceModel + '/' + deviceId,
    method: 'post',
    data: data
  };
  return new Promise((resolve, reject) => {
    resolve(promiseRequest(requestObj))
  })
}

// 口令更新
let devicePassword = (customerId, deviceId, data) => {
  let requestObj = {
    url: url.apiUrl.devicePassword + '/' + customerId + '/' + deviceId,
    method: 'post',
    data: data
  };
  return new Promise((resolve, reject) => {
    resolve(promiseRequest(requestObj))
  })
}

// 公众号文章
let introduce = (data) => {
  let requestObj = {
    url: url.apiUrl.introduce,
    method: 'get'
  };
  return new Promise((resolve, reject) => {
    resolve(promiseRequest(requestObj))
  })
}

// 公众号文章列表
let materials = (data) => {
  let requestObj = {
    url: url.apiUrl.materials,
    method: 'get'
  };
  return new Promise((resolve, reject) => {
    resolve(promiseRequest(requestObj))
  })
}

// 模式列表
let getUserModelList = (customerId, data) => {
  let requestObj = {
    url: url.apiUrl.customer + '/' + customerId + '/assets?' + params(data),
    method: 'get'
  };
  return new Promise((resolve, reject) => {
    resolve(promiseRequest(requestObj))
  })
}

// 列表
let getUserFormulaList = (customerId, data) => {
  let requestObj = {
    url: url.apiUrl.customer + '/' + customerId + '/assets?' + params(data),
    method: 'get'
  };
  return new Promise((resolve, reject) => {
    resolve(promiseRequest(requestObj))
  })
}

// 模式下发
let assignSteps = (modelId, data) => {
  let requestObj = {
    url: url.apiUrl.assignSteps + '/' + modelId,
    method: 'post',
    data: data
  };
  return new Promise((resolve, reject) => {
    resolve(promiseRequest(requestObj))
  })
}

let listRelationInfos = (deviceId )=>{
  let requestObj = {
    url: url.apiUrl.listRelationInfos + '?toId=' + deviceId + "&toType=DEVICE",
    method: 'get'
  };

  return new Promise((resolve, reject) => {
    resolve(promiseRequest(requestObj))
  })
}

export default {
  accountLogin: accountLogin,
  wxLogin: wxLogin,
  userBindWx: userBindWx,
  getOssToken: getOssToken,
  user: user,

  // 门店版
  userStore: userStore,
  storeDeviceList: storeDeviceList,
  bindStoreDevice: bindStoreDevice,
  unBindStoreDevice: unBindStoreDevice, 
  getDevice: getDevice,
  getDeviceByName:getDeviceByName,
  saveDevice: saveDevice,
  reportRepair: reportRepair,
  getModel: getModel,
  setDeviceModel: setDeviceModel,
  deleteNewModel: deleteNewModel,
  getDeviceModel: getDeviceModel,
  setDelayDeviceModel: setDelayDeviceModel,
  deleteDelayDeviceModel: deleteDelayDeviceModel,
  setDeviceHot: setDeviceHot,
  devicePassword: devicePassword,
  introduce: introduce,
  materials: materials,

  // 督导版
  getUserModelList: getUserModelList,
  getUserFormulaList: getUserFormulaList,
  assignSteps: assignSteps,
  listRelationInfos : listRelationInfos,
}