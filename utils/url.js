let host = 'https://iot.autata.com' // 设置API接口的ip地址和端口

let apiUrl = {
  oss: host + '/api/oss/token', // oss token
  accountLogin: host + '/api/auth/login', // 账号登录
  wxLogin: host + '/api/auth/login/code', // 微信登录
  user: host + '/api/user', // 账号
  userBindWx: host + '/api/user/wx', // 账号绑定微信
  customer: host + '/api/customer', // 用户
  relations: host + '/api/relations', // 设备门店关系
  relation: host + '/api/relation', // 设备门店关系
  device: host + '/api/device', // 设备
  asset: host + '/api/asset', // 模式(与平台服务交互)
  assignSteps: host + '/api/assignSteps', // 下发模式
  deviceModel: host + '/api/plugins/rpc/twoway', //设备模式(与设备交互)
  delayDeviceModel: host + '/api/plugins/rpc/subscribe', //设备模式(与设备交互)
  deleteDelayDeviceModel: host + '/api/plugins/rpc/unSubscribe',//取消预约模式
  devicePassword: host + '/api/plugins/rpc/password', // 口令
  deviceTelemetry: host + '/api/plugins/telemetry/DEVICE', // 模式更新之后,删除模式
  introduce: host + '/api/admin/settings/introduce', //公众号文章
  materials: host + '/api/admin/wx/materials', // 公众号文章列表

  listRelationInfos: host + "/api/relations/info",
}

let wsUrl = 'wss://iot.autata.com/api/ws/plugins/telemetry'
let cdnUrl = 'https://cdh.autata.com'

export default {
  apiUrl: apiUrl,
  wsUrl: wsUrl,
  cdnUrl: cdnUrl
}