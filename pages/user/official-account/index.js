// pages/user/official-account/index.js
import Toast from '@vant/weapp/toast/toast';
const api = require('./../../../utils/request.js').default;
const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    items: [],
  },

  onClickItem: function(event) {
    const url = event.currentTarget.dataset.url;
    wx.navigateTo({
      url: '../../webview/index?newsUrl=' + encodeURIComponent(url),
    })
  }, 

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    Toast.loading({
      duration: 0,
      message: '加载中...'
    });

    api.materials(null).then(res => {
      Toast.clear();
      if (res && res.statusCode == 200) {
        console.log(res);
        let items = res.data.items;
        items.forEach(item => {
          item.content.updateTimeFormat = item.content.updateTime.substring(0, 10);
        });
        this.setData({
          items: res.data.items
        });
      } else {
        Toast.fail("数据加载失败!");
      }
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})