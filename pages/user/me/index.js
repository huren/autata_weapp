
const api = require('./../../../utils/request.js').default;
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
    webUserInfo: {},
  },

  onClickBindAccount: function () {
    wx.navigateTo({
      url: '/pages/user/login/index',
    })
  },

  onClickMessage: function () {
    wx.navigateTo({
      url: '/pages/user/official-account/index',
    })
  },

  onClickBindAbout: function () {
    wx.navigateTo({
      url: '/pages/user/about/index',
    })
  },

  getUserDetail() {
    let webUserInfo = app.getWebUserInfo();
    if (webUserInfo && webUserInfo.userId) {
      api.user(webUserInfo.userId, null).then(res => {
        if (res && res.statusCode == 200) {
          webUserInfo.email = res.data.name;
          webUserInfo.permission = res.data.additionalInfo && res.data.additionalInfo.permission;
          app.saveWebUserInfo(webUserInfo);
  
          this.setData({
            webUserInfo: app.getWebUserInfo()
          });
        }
      });
    }
  },

  refreshWebUserInfo() {
    this.getUserDetail();

    this.setData({
      userInfo: app.getUserInfo(),
      webUserInfo: app.getWebUserInfo()
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.refreshWebUserInfo();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})