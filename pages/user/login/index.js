
import Toast from '@vant/weapp/toast/toast';
const api = require('./../../../utils/request.js').default;
const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    userName: '',
    password: ''
  },

  onChangeField(event) {
    // event.detail 为当前输入的值
    console.log(event.detail);
    let type = event.currentTarget.dataset['type'];
    this.setData({
      [type]: event.detail
    });
  },

  onClickUserLogin: function () {
    if (!this.data.userName || !this.data.password || this.data.userName.match(/^[ ]*$/) || this.data.password.match(/^[ ]*$/) ) {
      Toast.fail('账号或密码不为空');
      return;
    }

    Toast.loading({
      duration: 0, 
      message: '登录中...'
    });

    let params = {
      username: this.data.userName,
      password: this.data.password
    }
    api.accountLogin(params).then(res=>{
      console.log(res)
      if (res && res.statusCode == 200) {
        // 存储token
        app.saveToken(res.data.token);
        app.saveRefreshToken(res.data.refreshToken);
        // 将wx与平台用户绑定
        this.userBindWx();
      } else if (res && res.statusCode == 401 && res.data.errorCode == 10) {
        Toast.fail("账号或密码错误");
      } else if (res && res.statusCode == 400 && res.data.errorCode == 31) {
        Toast.fail("不支持该账号登录");
      } else {
        Toast.fail("登录失败");
      }
    })
  },

  userBindWx() {
    app.wxLogin(
    () => {
      let params = {
        code: app.getWxLoginCode()
      }
      
      api.userBindWx(params).then(res=>{
        console.log(res)
        if (res && res.statusCode == 200) {
          Toast.success("登录成功");

          let pages = getCurrentPages(); // 当前页面
          wx.navigateBack({
            success: function() {
              pages.forEach(page => {
                page && page.refreshWebUserInfo && page.refreshWebUserInfo();
                page && page.refreshData && page.refreshData();
              });
            }
          });
        } else {
          Toast.fail("登录失败");
        }
      })
    }, 
    () => {
      Toast.fail("登录失败");
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})