// pages/manager/formula-search/index.js

const api = require('./../../../utils/request.js').default;
const util = require("./../../../utils/util.js");
const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    searchValue: null,
    formulaType: null,
    formulaList: []
  },

  onChangeSearch(event) {
    this.setData({
      searchValue: event.detail
    });
    this.getUserFormulaList();
  },

  onCancelSearch(event) {
    wx.navigateBack();
  },

  getUserFormulaList() {
    let that = this;
    let customerId = app.getWebUserInfo().customerId;
    let params = {};
    params.type = this.data.formulaType;
    params.textSearch = this.data.searchValue;
    params.limit = 999999;
    params.page = 0
    api.getUserFormulaList(customerId, params).then(res => {
      console.log(res)
      if (res && res.statusCode == 200) {
        let formulas = res.data.data;
        if (formulas && formulas.length > 0) {
          formulas.forEach(item => {
            item.formatCreatedTime = util.formatDate2(item.createdTime)
          });
        }
        that.setData({
          formulaList: formulas
        });
      } 
    })
  }, 

  onClickItem: function(event) {
    let item = event.currentTarget.dataset.item;
    if (item && item.additionalInfo && item.additionalInfo.formulas) {
      wx.navigateTo({
        url: '/pages/manager/formula-detail/index?formulaId=' + JSON.stringify(item.id.id) + '&formulas=' + JSON.stringify(item.additionalInfo.formulas),
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let formulaType = JSON.parse(options.type);
    this.setData({
      formulaType: formulaType
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})