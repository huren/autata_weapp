// pages/manager/model-search/index.js

const api = require('./../../../utils/request.js').default;
const util = require("./../../../utils/util.js");
const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    searchValue: null,
    modelType: null,
    modelList: []
  },

  onChangeSearch(event) {
    this.setData({
      searchValue: event.detail
    });
    this.getUserModelList();
  },

  onCancelSearch(event) {
    wx.navigateBack();
  },

  getUserModelList() {
    let that = this;
    let customerId = app.getWebUserInfo().customerId;
    let params = {};
    params.type = this.data.modelType;
    params.textSearch = this.data.searchValue;
    params.limit = 9999999;
    params.page = 0
    api.getUserModelList(customerId, params).then(res => {
      console.log(res)
      if (res && res.statusCode == 200) {
        let models = res.data.data;
        if (models && models.length > 0) {
          models.forEach(item => {
            item.formatCreatedTime = util.formatDate2(item.createdTime)
          });
        }
        that.setData({
          modelList: models
        });
      } 
    })
  }, 

  onClickItem: function(event) {
    let item = event.currentTarget.dataset.item;
    if (item && item.additionalInfo && item.additionalInfo.steps) {
      wx.navigateTo({
        url: '/pages/manager/model-step/index?modelId=' + JSON.stringify(item.id.id) + '&steps=' + JSON.stringify(item.additionalInfo.steps),
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let modeType = JSON.parse(options.type);
    this.setData({
      modelType: modeType
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})