// pages/manager/model/index.js

import Toast from '@vant/weapp/toast/toast';

const api = require('./../../../utils/request.js').default;
const util = require("./../../../utils/util.js");
const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabActive: 0,
    modelList1: [],
    modelList2: [],
    modelList3: [],

    scroll1: {
      pagination: {
        page: 0,
        totalPage: 1,
        limit: 20,
        length: 20
      },
      empty: {
        img: './../../imgs/empty.png',
        text: '暂无模式数据'
      },
      refresh: {
        type: 'default',
        style: 'black',
        background: "#000"
      },
      loadmore: {
        type: 'default',
        icon: './../../imgs/loading.gif',
        background: '#FFF',
        title: {
          show: true,
          text: '加载中...',
          color: "#999"
        }
      }
    },

    scroll2: {
      pagination: {
        page: 0,
        totalPage: 1,
        limit: 20,
        length: 20
      },
      empty: {
        img: './../../imgs/empty.png',
        text: '暂无模式数据'
      },
      refresh: {
        type: 'default',
        style: 'black',
        background: "#000"
      },
      loadmore: {
        type: 'default',
        icon: './../../imgs/loading.gif',
        background: '#FFF',
        title: {
          show: true,
          text: '加载中...',
          color: "#999"
        }
      }
    },

    scroll3: {
      pagination: {
        page: 0,
        totalPage: 1,
        limit: 20,
        length: 20
      },
      empty: {
        img: './../../imgs/empty.png',
        text: '暂无模式数据'
      },
      refresh: {
        type: 'default',
        style: 'black',
        background: "#000"
      },
      loadmore: {
        type: 'default',
        icon: './../../imgs/loading.gif',
        background: '#FFF',
        title: {
          show: true,
          text: '加载中...',
          color: "#999"
        }
      }
    },

  },

  onChange(event) {
    let active = event.detail.name;
    this.setData({
      tabActive: active
    });
  },

  onClickSearch() {
    let types = ['STEP1', 'STEP2', 'STEP3'];
    wx.navigateTo({
      url: '/pages/manager/model-search/index?type=' + JSON.stringify(types[this.data.tabActive]),
    })
  },

  refresh1: function () {
    this.getUserModelList1('refresh', 0);
  },

  refresh2: function () {
    this.getUserModelList2('refresh', 0);
  },

  refresh3: function () {
    this.getUserModelList3('refresh', 0);
  },

  loadMore1: function () {
    this.getUserModelList1('loadMore', this.data.scroll1.pagination.page + 1)
  },
  
  loadMore2: function () {
    this.getUserModelList2('loadMore', this.data.scroll2.pagination.page + 1)
  },

  loadMore3: function () {
    this.getUserModelList3('loadMore', this.data.scroll3.pagination.page + 1)
  },

  getUserModelList1(loadType, page) {
    let that = this;
    let customerId = app.getWebUserInfo().customerId;
    let params = {};
    params.type = 'STEP1';
    params.limit = this.data.scroll1.pagination.limit;
    params.page = page
    api.getUserModelList(customerId, params).then(res => {
      console.log(res)
      if (res && res.statusCode == 200) {
        let models = res.data.data;
        if (models && models.length > 0) {
          models.forEach(item => {
            item.formatCreatedTime = util.formatDate2(item.createdTime)
          });
        }

        if (loadType == 'refresh') {
          let scroll = that.data.scroll1;
          scroll.pagination.page = page;
          scroll.pagination.totalPage = page + 1;
          scroll.pagination.length = models.length;
          that.setData({
            modelList1: models,
            scroll1: scroll
          });
        } else {
          let list = that.data.modelList1;
          that.setData({
            modelList1: list.concat(models)
          });

          let scroll = that.data.scroll1;
          scroll.pagination.page = page;
          if (res.data.hasNext) {
            scroll.pagination.totalPage = scroll.pagination.page + 1;
            scroll.pagination.length = that.data.modelList1.length;
          } else {
            scroll.pagination.totalPage = scroll.pagination.page;
            scroll.pagination.length = that.data.modelList1.length;
          }
          that.setData({
            scroll1: scroll
          });
        }
      } else {
        Toast.fail("数据获取失败!");
      }
    })
  }, 

  getUserModelList2(loadType, page) {
    let that = this;
    let customerId = app.getWebUserInfo().customerId;
    let params = {};
    params.type = 'STEP2';
    params.limit = this.data.scroll2.pagination.limit;
    params.page = page
    api.getUserModelList(customerId, params).then(res => {
      console.log(res)
      if (res && res.statusCode == 200) {
        let models = res.data.data;
        if (models && models.length > 0) {
          models.forEach(item => {
            item.formatCreatedTime = util.formatDate2(item.createdTime)
          });
        }

        if (loadType == 'refresh') {
          let scroll = that.data.scroll2;
          scroll.pagination.page = page;
          scroll.pagination.totalPage = page + 1;
          scroll.pagination.length = models.length;
          that.setData({
            modelList2: models,
            scroll2: scroll
          });
        } else {
          let list = that.data.modelList2;
          that.setData({
            modelList2: list.concat(models)
          });

          let scroll = that.data.scroll2;
          scroll.pagination.page = page;
          if (res.data.hasNext) {
            scroll.pagination.totalPage = scroll.pagination.page + 1;
            scroll.pagination.length = that.data.modelList2.length;
          } else {
            scroll.pagination.totalPage = scroll.pagination.page;
            scroll.pagination.length = that.data.modelList2.length;
          }
          that.setData({
            scroll2: scroll
          });
        }
      } else {
        Toast.fail("数据获取失败!");
      }
    })
  }, 

  getUserModelList3(loadType, page) {
    let that = this;
    let customerId = app.getWebUserInfo().customerId;
    let params = {};
    params.type = 'STEP3';
    params.limit = this.data.scroll3.pagination.limit;
    params.page = page
    api.getUserModelList(customerId, params).then(res => {
      console.log(res)
      if (res && res.statusCode == 200) {
        let models = res.data.data;
        if (models && models.length > 0) {
          models.forEach(item => {
            item.formatCreatedTime = util.formatDate2(item.createdTime)
          });
        } else {
          models = [];
        }

        if (loadType == 'refresh') {
          let scroll = that.data.scroll3;
          scroll.pagination.page = page;
          scroll.pagination.totalPage = page + 1;
          scroll.pagination.length = models.length;
          that.setData({
            modelList3: models,
            scroll3: scroll
          });
        } else {
          let list = that.data.modelList3;
          that.setData({
            modelList3: list.concat(models)
          });

          let scroll = that.data.scroll3;
          scroll.pagination.page = page;
          if (res.data.hasNext) {
            scroll.pagination.totalPage = scroll.pagination.page + 1;
            scroll.pagination.length = that.data.modelList3.length;
          } else {
            scroll.pagination.totalPage = scroll.pagination.page;
            scroll.pagination.length = that.data.modelList3.length;
          }
          that.setData({
            scroll3: scroll
          });
        }
      } else {
        Toast.fail("数据获取失败!");
      }
    })
  }, 

  onClickItem: function(event) {
    let item = event.currentTarget.dataset.item;
    if (item && item.additionalInfo && item.additionalInfo.steps) {
      wx.navigateTo({
        url: '/pages/manager/model-step/index?modelId=' + JSON.stringify(item.id.id) + '&steps=' + JSON.stringify(item.additionalInfo.steps),
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getUserModelList1('refresh', this.data.scroll1.pagination.page);
    this.getUserModelList2('refresh', this.data.scroll2.pagination.page);
    this.getUserModelList3('refresh', this.data.scroll3.pagination.page);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})