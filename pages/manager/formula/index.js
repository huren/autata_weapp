// pages/manager/formula/index.js

const api = require('./../../../utils/request.js').default;
const util = require("./../../../utils/util.js");
const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabActive: 0,
    formulaList1: [],
    formulaList2: [],

    scroll1: {
      pagination: {
        page: 0,
        totalPage: 1,
        limit: 20,
        length: 20
      },
      empty: {
        img: './../../imgs/empty.png',
        text: '暂无配方数据'
      },
      refresh: {
        type: 'default',
        style: 'black',
        background: "#000"
      },
      loadmore: {
        type: 'default',
        icon: './../../imgs/loading.gif',
        background: '#FFF',
        title: {
          show: true,
          text: '加载中...',
          color: "#999"
        }
      }
    },

    scroll2: {
      pagination: {
        page: 0,
        totalPage: 1,
        limit: 20,
        length: 20
      },
      empty: {
        img: './../../imgs/empty.png',
        text: '暂无配方数据'
      },
      refresh: {
        type: 'default',
        style: 'black',
        background: "#000"
      },
      loadmore: {
        type: 'default',
        icon: './../../imgs/loading.gif',
        background: '#FFF',
        title: {
          show: true,
          text: '加载中...',
          color: "#999"
        }
      }
    },

  },

  onChange(event) {
    let active = event.detail.name;
    this.setData({
      tabActive: active
    });
  },

  onClickSearch() {
    let types = ['WDG', 'WDP'];
    wx.navigateTo({
      url: '/pages/manager/formula-search/index?type=' + JSON.stringify(types[this.data.tabActive]),
    })
  },
  
  onClickItem: function (event) {
    let item = event.currentTarget.dataset.item;
    if (item && item.additionalInfo && item.additionalInfo.formulas) {
      wx.navigateTo({
        url: '/pages/manager/formula-detail/index?formulaId=' + JSON.stringify(item.id.id) + '&formulas=' + JSON.stringify(item.additionalInfo.formulas),
      })
    }
  },

  refresh1: function () {
    this.getUserFormulaList1('refresh', 0);
  },

  refresh2: function () {
    this.getUserFormulaList2('refresh', 0);
  },

  loadMore1: function () {
    this.getUserFormulaList1('loadMore', this.data.scroll1.pagination.page + 1)
  },
  
  loadMore2: function () {
    this.getUserFormulaList2('loadMore', this.data.scroll2.pagination.page + 1)
  },

  getUserFormulaList1(loadType, page) {
    let that = this;
    let customerId = app.getWebUserInfo().customerId;
    let params = {};
    params.type = 'WDG';
    params.limit = this.data.scroll1.pagination.limit;
    params.page = page
    api.getUserFormulaList(customerId, params).then(res => {
      console.log(res)
      if (res && res.statusCode == 200) {
        let formulas = res.data.data;
        if (formulas && formulas.length > 0) {
          formulas.forEach(item => {
            item.formatCreatedTime = util.formatDate2(item.createdTime)
          });
        }

        if (loadType == 'refresh') {
          let scroll = that.data.scroll1;
          scroll.pagination.page = page;
          scroll.pagination.totalPage = page + 1;
          scroll.pagination.length = formulas.length;
          that.setData({
            formulaList1: formulas,
            scroll1: scroll
          });
        } else {
          let list = that.data.formulaList1;
          that.setData({
            formulaList1: list.concat(formulas)
          });

          let scroll = that.data.scroll1;
          scroll.pagination.page = page;
          if (res.data.hasNext) {
            scroll.pagination.totalPage = scroll.pagination.page + 1;
            scroll.pagination.length = that.data.formulaList1.length;
          } else {
            scroll.pagination.totalPage = scroll.pagination.page;
            scroll.pagination.length = that.data.formulaList1.length;
          }
          that.setData({
            scroll1: scroll
          });
        }

      } 
    })
  }, 

  getUserFormulaList2(loadType, page) {
    let that = this;
    let customerId = app.getWebUserInfo().customerId;
    let params = {};
    params.type = 'WDP';
    params.limit = this.data.scroll2.pagination.limit;
    params.page = page
    api.getUserFormulaList(customerId, params).then(res => {
      console.log(res)
      if (res && res.statusCode == 200) {
        let formulas = res.data.data;
        if (formulas && formulas.length > 0) {
          formulas.forEach(item => {
            item.formatCreatedTime = util.formatDate2(item.createdTime)
          });
        }

        if (loadType == 'refresh') {
          let scroll = that.data.scroll2;
          scroll.pagination.page = page;
          scroll.pagination.totalPage = page + 1;
          scroll.pagination.length = formulas.length;
          that.setData({
            formulaList2: formulas,
            scroll2: scroll
          });
        } else {
          let list = that.data.formulaList2;
          that.setData({
            formulaList2: list.concat(formulas)
          });

          let scroll = that.data.scroll2;
          scroll.pagination.page = page;
          if (res.data.hasNext) {
            scroll.pagination.totalPage = scroll.pagination.page + 1;
            scroll.pagination.length = that.data.formulaList2.length;
          } else {
            scroll.pagination.totalPage = scroll.pagination.page;
            scroll.pagination.length = that.data.formulaList2.length;
          }
          that.setData({
            scroll2: scroll
          });
        }

      } 
    })
  }, 

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getUserFormulaList1('refresh', this.data.scroll1.pagination.page);
    this.getUserFormulaList2('refresh', this.data.scroll2.pagination.page);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})