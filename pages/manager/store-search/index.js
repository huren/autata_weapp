// pages/manager/store-search/index.js
const api = require('./../../../utils/request.js').default;
const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    searchValue: '',
    storeList: [],
    result: []
  },

  onChangeSearch(event) {
    let searchValue = event.detail
    let result = [];
    if (searchValue == "") {
      searchValue = null;
    }
    if (searchValue || searchValue == 0) {
      if (this.data.storeList.length > 0) {
        result = this.data.storeList.filter(item => item.name.includes(searchValue));
      }
    } 
    this.setData({
      result: result
    });

    // this.getStoreList();
  },

  onCancelSearch(event) {
    wx.navigateBack();
  },

  // getStoreList() {
  //   let params = {};
  //   params.fromId = app.getWebUserInfo().userId;
  //   params.fromType = 'USER';
  //   api.userStore(params).then(res => {
  //     console.log(res)
  //     if (res && res.statusCode == 200) {
  //       let relations = res.data;
  //       if (relations && relations.length > 0) {
  //         let storeList = [];
  //         relations.forEach(item => {
  //           storeList.push(item.toEntity);
  //         });
  //         this.setData({
  //           storeList: storeList
  //         });
  //       }
  //     } 
  //   })
  // },

  onClickItem: function(event) {
    let item = event.currentTarget.dataset.item;
    if (item) {
      wx.navigateTo({
        url: '/pages/manager/store/index?store=' + JSON.stringify(item),
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let storeList = JSON.parse(options.storeList);
    this.setData({
      storeList: storeList
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})