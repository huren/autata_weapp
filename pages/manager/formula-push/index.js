// pages/manager/formula-push/index.js

import Toast from '@vant/weapp/toast/toast';

const api = require('./../../../utils/request.js').default;
const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    formulaId: null,
    storeList:[],
    list: [],
    result: []
  },

  onChange(event) {
    this.setData({
      result: event.detail
    });
  },

  toggle(event) {
    const { index } = event.currentTarget.dataset;
    const checkbox = this.selectComponent(`.checkboxes-${index}`);
    checkbox.toggle();
  },

  noop() {},

  getStoreList() {
    Toast.loading({
      duration: 0,
      message: '加载中...'
    });

    // 获取门店列表
    let params = {};
    params.fromId = app.getWebUserInfo().userId;
    params.fromType = 'USER';
    api.userStore(params).then(res => {
      console.log(res)
      if (res && res.statusCode == 200) {
        let relations = res.data;
        if (relations && relations.length > 0) {
          let storeList = [];
          let list = [];
          let index = 1;
          relations.forEach(item => {
            storeList.push(item.toEntity);
            list.push(index++);
          });
          this.setData({
            storeList: storeList,
            list: list
          });
        } 
        Toast.clear();
      } else {
        Toast.fail("门店获取失败");
      }
    })
  },

  onClickPush() {
    if (this.data.result.length > 0) {
      let selectedStoreList = [];
      this.data.result.forEach(index => {
        selectedStoreList.push(this.data.storeList[index - 1].id.id);
      });

      if (selectedStoreList.length > 0) {
        let formulaId = this.data.formulaId;
        let params = {};
        params.stores = selectedStoreList;
        api.assignSteps(formulaId, params).then(res => {
          console.log(res)
          if (res && res.statusCode == 200) {
            Toast({
              type: 'success',
              message: '下发成功!',
              onClose: () => {
                wx.navigateBack({
                  delta: 2
                });
              },
            });
          } else {
            Toast.fail("下发失败!");
          }
        })
      }
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let pxToRpxScale = 750 / wx.getSystemInfoSync().windowWidth;
    this.setData({
      // 120rpx 是底部按钮所在view的高度
      windowHeight: wx.getSystemInfoSync().windowHeight - (120 / pxToRpxScale)
    })

    let formulaId = JSON.parse(options.formulaId);
    this.setData({
      formulaId: formulaId
    });
    this.getStoreList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})