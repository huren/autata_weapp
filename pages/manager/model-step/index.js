// pages/manager/model-step/index.js

import Toast from '@vant/weapp/toast/toast';

const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    windowHeight: 0,
    active: 0,
    modelId: null,
    steps: [],
    enableSteps: false,

    // 工序
    funName: {
      0: "空闲",
      1: "进水",
      2: "煮水",
      3: "煮料",
      4: "焖料",
      5: "保温",
      6: "蜜黑糖",
      7: "洗料",
      8: "停止",
      9: "排水"
    },

    // 搅拌
    stir: {
      0: {
        "name": "不搅拌"
      },
      1: {
        "name": "单向搅拌"
      },
      2: {
        "name": "双向搅拌"
      }
    },

    // 火力
    fire: {
      0: {
        "name": "无"
      },
      1: {
        "name": "小火"
      },
      2: {
        "name": "中火"
      },
      3: {
        "name": "大火"
      }
    }
  },

  onClickStep: function (event) {
    this.setData({
      active: event.detail
    })
  },

  onClickSelectStore: function (event) {
    wx.navigateTo({
      url: '/pages/manager/model-push/index?modelId=' + JSON.stringify(this.data.modelId),
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let pxToRpxScale = 750 / wx.getSystemInfoSync().windowWidth;
    this.setData({
      windowHeight: wx.getSystemInfoSync().windowHeight
    })

    let modelId = JSON.parse(options.modelId);
    let steps = JSON.parse(options.steps);
    for (let index = 0; index < steps.length; index++) {
      let item = steps[index]; 
      item.stepName = (item.funName || item.funName === 0) && this.data.funName[item.funName];
      item.confirmText = item.confirm == 1 ? '是' : '否';
      item.fixTempetureText = item.fixTempeture == 1 ? '是' : '否';
      item.powerText = (item.power || item.power === 0) && this.data.fire[item.power].name;
      item.remindText = item.remind == 1 ? '是' : '否';
      item.stirText = (item.stir || item.stir === 0) && this.data.stir[item.stir].name;
      let sIndex = index+1;
      item.text = '工序' + sIndex;
      item.desc = item.stepName;
    }

    this.setData({
      modelId: modelId,
      steps: steps
    });

    let webUserInfo = app.getWebUserInfo();
    if (webUserInfo.permission && webUserInfo.permission == 'LAGROT') {
      this.setData({
        enableSteps: true
      });
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})