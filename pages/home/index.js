// pages/home/index.js

import Toast from '@vant/weapp/toast/toast';

const api = require('./../../utils/request.js').default;
const url = require('./../../utils/url.js').default;
const app = getApp();
let webSocket = null;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
    webUserInfo: {},
    isLogin: false,
    isManager: true, // 是否为督导

    // 店长
    store: {},
    storeDeviceList: [],
    deviceList: [],
    deviceType: null,
    defaultDeviceType: '门店设备',
    searchValue: "",
    activeNames: [],
    socketOpen: false,
    // websocket 订阅次数
    wsTopicCount: 0,

    // 督导
    storeList: []
    
  },

  onClickAvatar: function (e) {
    wx.navigateTo({
      url: '/pages/user/me/index',
    })
  },

  onClickMessage: function () {
    api.introduce(null).then(res => {
      if (res && res.statusCode == 200) {
        console.log(res);
        // 跳转公众号
        let newsUrl = 'https://' + res.data.jsonValue.baseUrl;
        wx.navigateTo({
          url: '/pages/webview/index?newsUrl=' + encodeURIComponent(newsUrl),
        })
      }
    });
  },

  onClickGoLogin: function () {
    wx.navigateTo({
      url: '/pages/user/login/index',
    })
  },

  login() {
    if (!app.getWxLoginCode()) {
      return;
    }

    let params = {
      code: app.getWxLoginCode()
    };
    api.wxLogin(params).then(res => {
      if (res && res.statusCode == 200) {
        console.log('web平台登录成功！');
        // 存储token
        app.saveToken(res.data.token);
        app.saveRefreshToken(res.data.refreshToken);

        this.refreshWebUserInfo();
        this.refreshData();
      }
    })
  },

  getUserDetail() {
    let webUserInfo = app.getWebUserInfo();
    if (webUserInfo && webUserInfo.userId) {
      api.user(webUserInfo.userId, null).then(res => {
        if (res && res.statusCode == 200) {
          webUserInfo.email = res.data.name;
          webUserInfo.permission = res.data.additionalInfo && res.data.additionalInfo.permission;
          app.saveWebUserInfo(webUserInfo);
  
          this.setData({
            webUserInfo: app.getWebUserInfo()
          });
        }
      });
    }
  },

  refreshWebUserInfo() {
    this.getUserDetail();

    this.setData({
      webUserInfo: app.getWebUserInfo(),
      isManager: app.getWebUserInfo().isManager,
      isLogin: app.isLogin()
    });
  },

  refreshData(hideLoading) {
    if (this.data.isManager) {
      this.getStoreList(hideLoading);
    } else {
      this.getStoreAndStoreDeviceList(hideLoading);
    }
  },

  /**************** 店长 ****************************************/
  onChangeStoreDeviceCollapse: function (event) {
    this.setData({
      activeNames: event.detail
    });
  },

  validDevice(deviceId){
    return new Promise((resolve, reject) => {
      api.listRelationInfos(deviceId).then(res => {
        if (res && res.statusCode == 200) {
          if(res.data.length > 0){
            resolve(res.data[0].fromName);
          }else{
            resolve("");
          }
        } else {
          reject();
        }
      })
    })
     
  },

  getDeviceDetail(deviceId) {
    Toast.loading({
      duration: 0,
      message: '加载中...'
    });

    let params = {};
    params.id = deviceId;
    api.getDeviceByName(params).then(res => {
      if (res && res.statusCode == 200) {
        //校验设备是否在该门店下
        let storeName = this.data.store.name;
        this.validDevice(res.data.id.id).then(rs=>{
          let isBind = "" !== rs;
          if(!isBind || rs === storeName){
            Toast.clear();
            wx.navigateTo({
              url: '/pages/store/bind-device/index?deviceId=' + JSON.stringify(deviceId) + '&isBind=' + isBind,
            })
          }else{
            Toast.fail("设备已分配给" + rs);
          }
        })
       
      } else {
        console.error(res);
        Toast.fail("设备详情获取失败!");
      }
    })
  },
  onClickMenu: function () {
    let that_ = this;
    wx.scanCode({
      success(res) {
        console.log(res);
        if (res) {
          let deviceId = res.result;
          that_.getDeviceDetail(deviceId);
         
        }
      },
      fail(res) {
        console.log(res);
      },
      complete(res) {
      }
    })
  },

  onClickDeviceItem: function (event) {
    let deviceInfo = this.data.storeDeviceList.filter(item => item.id.id  == event.currentTarget.dataset.deviceId);
    if (deviceInfo && deviceInfo.length > 0 && deviceInfo[0].type == '煮料机器人') {
      wx.navigateTo({
        url: '/pages/store/cooking-device/index?deviceInfo=' + JSON.stringify(deviceInfo[0]),
      })
    } else {
      wx.navigateTo({
        url: '/pages/store/poder-device/index?deviceInfo=' + JSON.stringify(deviceInfo[0]),
      })
    }
  },

  getStoreAndStoreDeviceList(hideLoading) {
    if (this.data.isLogin) {
      // 被其他页面回调时,可能不需要loading
      if (!hideLoading) {
        Toast.loading({
          duration: 0,
          message: '加载中...'
        });
      }

      // 获取门店列表
      let params = {};
      params.fromId = this.data.webUserInfo.userId;
      params.fromType = 'USER';
      api.userStore(params).then(res => {
        console.log(res)
        if (res && res.statusCode == 200) {
          let relations = res.data;
          if (relations && relations.length > 0) {
            let store = relations[0].toEntity;
            this.setData({
              store: store
            })
            app.saveStoreInfo(store);
            if (store && Object.keys(store).length != 0) {
              this.getStoreDeviceList();
            } else {
              Toast.clear();
            }
          } else {
            Toast.clear();
          }
        } else {
          Toast.fail("门店获取失败");
        }
      })
    }
  },

  getStoreDeviceList() {
    let store = this.data.store;
    let params = {};
    params.fromId = store.id.id;
    params.fromType = store.id.entityType;
    params.toType = 'DEVICE'
    api.storeDeviceList(params).then(res => {
      console.log(res)
      if (res && res.statusCode == 200) {
        let storeDeviceList = [];
        let relations = res.data;
        if (relations && relations.length > 0) {
          let index = 0;
          relations.forEach(item => {
            // WebSocket 数据
            item.toEntity.index = ++index;
            item.toEntity.isOnline = false;

            storeDeviceList.push(item.toEntity);
          });
        }
        this.setData({
          storeDeviceList: storeDeviceList,
        });
        this.connectWebSocket();
        this.filterDeviceList(this.data.defaultDeviceType);
        Toast.clear();
        // Toast.success("门店设备获取成功");
      } else {
        Toast.clear();
        // Toast.fail("门店设备获取失败");
      }
    })
  },

  filterDeviceList: function (type) {
    if (this.data.defaultDeviceType == type) {
      this.setData({
        deviceType: type,
        deviceList: this.data.storeDeviceList
      })
    } else {
      this.setData({
        deviceType: type,
        deviceList: this.data.storeDeviceList ? this.data.storeDeviceList.filter(item => item.type == type) : []
      })
    }
  }, 

  onClickStoreDeviceType: function (event) {
    let type = event.currentTarget.dataset['type'];
    this.filterDeviceList(type);
  },

  connectWebSocket() {
    if (!this.data.storeDeviceList || this.data.storeDeviceList.length == 0) {
      return;
    }

    let that = this;
    webSocket = wx.connectSocket({
      url: url.wsUrl + '?token=' + app.getToken(),
      timeout: 60000,
      success: function(res) {
        console.log("WebSocket连接成功! res = " + JSON.stringify(res));
      }, 
      fail: function(res) {
        console.error("WebSocket连接失败! res = " + JSON.stringify(res));
      }
    })

    webSocket.onOpen(function(res) {
      console.info("WebSocket开启! res = " + JSON.stringify(res));
      const wsTopicCount = that.data.wsTopicCount;
      that.setData({
        socketOpen: true,
        wsTopicCount: wsTopicCount + 1
      });
      // 发送订阅消息
      that.sendWebSocketMessage();
    })

    webSocket.onMessage(function(res) {
      console.log("WebSocket接收服务器消息! res = " + JSON.stringify(res));
      if (res && res.data) {
        let result = JSON.parse(res.data);
        if (result.errorCode == 0 && result.data) {
          let storeDeviceList = that.data.storeDeviceList;
          storeDeviceList.forEach(item => {
            if (item.index == result.subscriptionId && result.data.active && result.data.active.length > 0) {
              try {
                item.isOnline = JSON.parse(result.data.active[0][1]);
              } catch (error) {
                console.error(error);
                item.isOnline = false;
              }
            }
          });
          that.setData({
            storeDeviceList: storeDeviceList
          });
          console.log(that.data.storeDeviceList);
          that.filterDeviceList(that.data.deviceType);
          console.log(that.data.deviceList);
        }
      }
    })

    webSocket.onError(function(res) {
      console.error("WebSocket错误! res = " + JSON.stringify(res));
    })
  },

  sendWebSocketMessage() {
    if (this.data.socketOpen && webSocket) {
      let attrSubCmds = [];
      this.data.storeDeviceList.forEach(item => {
        // console.log('item = ' + JSON.stringify(item));
        let attrSubCmd = {};
        attrSubCmd.cmdId = item.index;
        attrSubCmd.entityId = item.id.id;
        attrSubCmd.entityType = item.id.entityType;
        attrSubCmd.scope = "SERVER_SCOPE";
        attrSubCmds.push(attrSubCmd);
      });
      let msg = {};
      msg.attrSubCmds = attrSubCmds;
      console.log('WebSocket发送给服务端的消息! msg = ' + JSON.stringify(msg));
      webSocket.send({
        data: JSON.stringify(msg)
      })
    } else {
      console.error('WebSocket已关闭! 不能发送消息');
    }
  },

  closeWebSocket() {
    if (this.data.socketOpen && webSocket) {
      webSocket.close(function(res) {
        console.log('WebSocket已关闭！')
      })
    }
  },
  /**************** 店长 ****************************************/


  /**************** 督导 ****************************************/

  onClickSearch() {
    wx.navigateTo({
      url: '/pages/manager/store-search/index?storeList=' + JSON.stringify(this.data.storeList),
    })
  },

  getStoreList(hideLoading) {
    if (this.data.isLogin) {
      // 被其他页面回调时,可能不需要loading
      if (!hideLoading) {
        Toast.loading({
          duration: 0,
          message: '加载中...'
        });
      }

      // 获取门店列表
      let params = {};
      params.fromId = this.data.webUserInfo.userId;
      params.fromType = 'USER';
      api.userStore(params).then(res => {
        console.log(res)
        if (res && res.statusCode == 200) {
          let relations = res.data;
          if (relations && relations.length > 0) {
            let storeList = [];
            relations.forEach(item => {
              storeList.push(item.toEntity);
            });
            this.setData({
              storeList: storeList
            });
          } 
          Toast.clear();
        } else {
          Toast.fail("门店获取失败");
        }
      })
    }
  },

  onClickItem: function(event) {
    let item = event.currentTarget.dataset.item;
    if (item) {
      wx.navigateTo({
        url: '../manager/store/index?store=' + JSON.stringify(item),
      })
    }
  },

  onClickMenuModel: function() {
    wx.navigateTo({
      url: '/pages/manager/model/index',
    })
  },

  onClickMenuFormula: function() {
    wx.navigateTo({
      url: '/pages/manager/formula/index',
    })
  },


  /**************** 督导 ****************************************/

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      userInfo: app.getUserInfo(),
      wsTopicCount: 0
    });

    if (app.isLogin() == true) {
      this.refreshWebUserInfo();
      this.refreshData();
    } else {
      this.login();
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (wx.canIUse('hideHomeButton')) {
      wx.hideHomeButton();
    }

    const wsTopicCount = this.data.wsTopicCount;
    if (app.isLogin() == true && wsTopicCount >= 1) {
      this.connectWebSocket();
    } 
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    this.closeWebSocket();
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }

})