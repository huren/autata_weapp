//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    motto: '智能控制',
    subMotto: '设备状态随时掌握',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },
  //事件处理函数
  bindViewTap: function () {
    wx.navigateTo({
      url: '/pages/logs/logs'
    })
  },
  onLoad: function () {
    if (app.getUserInfo()) {
      this.setData({
        userInfo: app.getUserInfo(),
        hasUserInfo: true
      })

      this.gotoHome();
    } else if (this.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.saveUserInfo(res.userInfo);
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    }
  },
  getUserInfo: function (e) {
    console.log('点击获取用户信息');
    console.log(e)
    if (e.detail && e.detail.userInfo) {
      app.saveUserInfo(e.detail.userInfo);
      this.setData({
        userInfo: e.detail.userInfo,
        hasUserInfo: true
      })

      this.gotoHome();
    }
  },

  gotoHome() {
    // 跳转首页
    setTimeout(function () {
      wx.redirectTo({
        url: '/pages/home/index',
        // url: '/pages/store/home/index',
        // url: '/pages/manager/home/index',
      })
     }, 1000);
  }
})