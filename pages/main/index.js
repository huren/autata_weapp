// pages/home/index.js

import Toast from '@vant/weapp/toast/toast';

const api = require('./../../utils/request.js').default;
const url = require('./../../utils/url.js').default;
const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
    webUserInfo: {},
    isLogin: false,
    isManager: true, // 是否为督导
    //文章列表
    items: []
    

  },

  onClickAvatar: function (e) {
    wx.navigateTo({
      url: '/pages/user/me/index',
    })
  },

  onClickMessage: function () {
    api.introduce(null).then(res => {
      if (res && res.statusCode == 200) {
        console.log(res);
        // 跳转公众号
        let newsUrl = 'https://' + res.data.jsonValue.baseUrl;
        wx.navigateTo({
          url: '/pages/webview/index?newsUrl=' + encodeURIComponent(newsUrl),
        })
      }
    });
  },

  onClickGoLogin: function () {
    wx.navigateTo({
      url: '/pages/user/login/index',
    })
  },

  login() {
    if (!app.getWxLoginCode()) {
      return;
    }

    let params = {
      code: app.getWxLoginCode()
    };
    api.wxLogin(params).then(res => {
      if (res && res.statusCode == 200) {
        console.log('web平台登录成功！');
        // 存储token
        app.saveToken(res.data.token);
        app.saveRefreshToken(res.data.refreshToken);

        this.refreshWebUserInfo();
        this.refreshData();
      }
    })
  },

  refreshWebUserInfo() {
    this.setData({
      webUserInfo: app.getWebUserInfo(),
      isManager: app.getWebUserInfo().isManager,
      isLogin: app.isLogin()
    });
  },



  

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    api.materials(null).then(res => {
      if (res && res.statusCode == 200) {
        let items = res.data.items;
        items.forEach(item => {
          item.content.updateTimeFormat = item.content.updateTime.substring(0, 10);
        });
        this.setData({
          items: res.data.items
        });
      } else {
        Toast.fail("数据加载失败!");
      }
    });



    this.setData({
      userInfo: app.getUserInfo(),
    });

    if (app.isLogin() == true) {
      this.refreshWebUserInfo();
    } else {
      this.login();
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (wx.canIUse('hideHomeButton')) {
      wx.hideHomeButton();
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }

})