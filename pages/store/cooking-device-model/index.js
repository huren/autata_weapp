// pages/store/cooking-device-model/index.js
import Toast from '@vant/weapp/toast/toast';
const api = require('./../../../utils/request.js').default;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    windowHeight: 0,
    deviceId: null,
    tabActive: 0,
    active1: 0,
    active2: 0,
    active3: 0,
    steps1: null,
    steps2: null,
    steps3: null,

    // 工序
    funName: {
      0: "空闲",
      1: "进水",
      2: "煮水",
      3: "煮料",
      4: "焖料",
      5: "保温",
      6: "蜜黑糖",
      7: "洗料",
      8: "停止",
      9: "排水",
      16:"等待加料",
      17:"等待加糖"
    },

    // 搅拌
    stir: {
      0: {
        "name": "不搅拌"
      },
      1: {
        "name": "单向搅拌"
      },
      2: {
        "name": "双向搅拌"
      }
    },

    // 火力
    fire: {
      0: {
        "name": "无"
      },
      1: {
        "name": "小火"
      },
      2: {
        "name": "中火"
      },
      3: {
        "name": "大火"
      }
    },
  },

  onChange(event) {
    let active = event.detail.name;
    if (active == 0) {
      if (!this.data.steps1) {
        this.getDeviceModel(1);
      }
    } else if (active == 1) {
      if (!this.data.steps2) {
        this.getDeviceModel(2);
      }
    } else {
      if (!this.data.steps3) {
        this.getDeviceModel(3);
      }
    }
  },

  getDeviceModel(workMode) {
    Toast.loading({
      duration: 0,
      message: '加载中...'
    });

    let deviceId = this.data.deviceId;
    let params = {};
    let subParams = {};
    subParams.workMode = workMode;
    params.method = "GET_PROCESS";
    params.params = subParams;
    params.timeout = 100000;
    
    api.getDeviceModel(deviceId, params).then(res => {
      Toast.clear();
      if (res && res.statusCode == 200) {
        console.log(res);
        let steps = res.data.steps;
        if (steps) {
          let index = 1;
          steps.forEach(item => {
            item.stepName = (item.funName || item.funName === 0) && this.data.funName[item.funName];
            item.confirmText = item.confirm == 1 ? '是' : '否';
            item.fixTempetureText = item.fixTempeture == 1 ? '是' : '否';
            item.powerText = (item.power || item.power === 0) && this.data.fire[item.power].name;
            item.remindText = item.remind == 1 ? '是' : '否';
            item.stirText = (item.stir || item.stir === 0) && this.data.stir[item.stir].name;
            
            item.text = '工序' + (index++);
            item.desc = item.stepName;
          });
        }
        if (workMode == 1) {
          this.setData({
            steps1: steps
          });
        } else if (workMode == 2) {
          this.setData({
            steps2: steps
          });
        } else {
          this.setData({
            steps3: steps
          });
        }
        
      } else {
        Toast.fail("数据获取失败!");
      }
    });
  },

  onClickStep1: function (event) {
    this.setData({
      active1: event.detail
    })
  },

  onClickStep2: function (event) {
    this.setData({
      active2: event.detail
    })
  },

  onClickStep3: function (event) {
    this.setData({
      active3: event.detail
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let deviceId = JSON.parse(options.deviceId);
    this.setData({
      windowHeight: wx.getSystemInfoSync().windowHeight,
      deviceId: deviceId
    });
    this.getDeviceModel(1);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})