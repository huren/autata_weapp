// pages/store/cooking-device-model-step/index.js

const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    windowHeight: 0,
    active: 0,
    steps: [],
    enableSteps: false
  },

  onClickStep: function (event) {
    this.setData({
      active: event.detail
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let pxToRpxScale = 750 / wx.getSystemInfoSync().windowWidth;
    this.setData({
      windowHeight: wx.getSystemInfoSync().windowHeight
    })

    let currentStep = JSON.parse(options.currentStep) +  1;
    let steps = JSON.parse(options.steps);
    for (let index = 0; index < steps.length; index++) {
      let item = steps[index]; 
      let sIndex = index + 1;
      item.text = '工序' + sIndex;
      item.desc = item.stepName;
    }

    this.setData({
      active: parseInt(parseInt(currentStep) - 1),
      steps: steps
    });

    let webUserInfo = app.getWebUserInfo();
    if (webUserInfo.permission && webUserInfo.permission == 'LAGROT') {
      this.setData({
        enableSteps: true
      });
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})