// pages/store/bind-device/index.js

import Toast from '@vant/weapp/toast/toast';
const util = require("./../../../utils/util.js");
const api = require('./../../../utils/request.js').default;
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    deviceId: null,
    isBind: false,
    device: {},
    option1: [{
        text: '咖啡粉',
        value: 0
      },
      {
        text: '豆奶粉',
        value: 1
      },
      {
        text: '蜜料',
        value: 2
      },
    ],
    value1: 0,
  },




  getDeviceDetail() {
    Toast.loading({
      duration: 0,
      message: '加载中...'
    });

    let params = {};
    params.id = this.data.deviceId;
    api.getDeviceByName(params).then(res => {
      if (res && res.statusCode == 200) {
        //校验设备是否在该门店下
        Toast.clear();
        res.data.formatProductionDate = res.data.productionDate ? util.formatDate(res.data.productionDate) : null;
        this.setData({
          device: res.data
        });
      } else {
        console.error(res);
        Toast.fail("设备详情获取失败!");
      }
    })
  },

  saveDevice() {
    // 先保存设备,再绑定设备
    Toast.loading({
      duration: 0,
      message: '绑定中...'
    });

    let params = this.data.device;
    api.saveDevice(params).then(res => {
      if (res && res.statusCode == 200) {
        console.log(res);
        res.data.formatProductionDate = res.data.productionDate ? util.formatDate(res.data.productionDate) : null;
        this.setData({
          device: res.data
        });

        // 绑定门店设备
        this.bindStoreDevice();
      } else {
        console.error(res);
        Toast.fail("绑定设备失败!");
      }
    })
  },

  bindStoreDevice() {
    let that = this;
    let params = {};
    params.additionalInfo = null;
    params.from = {
      entityType: app.getStoreInfo().id.entityType,
      id: app.getStoreInfo().id.id
    };
    params.to = {
      entityType: this.data.device.id.entityType,
      id: this.data.device.id.id
    };
    params.type = "Contains";
    api.bindStoreDevice(params).then(res => {
      if (res && res.statusCode == 200) {
        console.log(res);
        Toast.success("绑定设备成功!");
        this.setData({
          isBind : true
        });
        this.refreshStoreDeviceList();
      } else if (res && res.statusCode == 403) {
        console.error(res);
        Toast.fail("您没有绑定此设备的权限!");
      } else {
        console.error(res);
        Toast.fail("绑定设备失败!");
      }
    })
  },

  // 刷新首页设备列表
  refreshStoreDeviceList() {
    let pages = getCurrentPages(); // 当前页面
    let beforePage = pages[pages.length - 2]; // 前一个页面
    beforePage && beforePage.refreshData && beforePage.refreshData(true);
  }, 

  onSaveAndBindDeivce: function () {
    let device = this.data.device;
    if (!device.label) {
      Toast("设备名称不能为空!");
      return;
    }
    this.saveDevice();
  },

  onConnectDevice: function (event) {
    if (this.data.device && this.data.device.batchNum) {
      let apSsid = this.data.device.batchNum.length > 16 ? this.data.device.batchNum.substring(0, 16) : this.data.device.batchNum;
      let step1 = "按配网键进入小程序配网，该设备AP SSID：" + apSsid;
      let step2 = "确保手机已连接Wi-Fi";
      let step3 = "输入Wi-Fi密码后连接";
      let guideSteps = [step1, step2, step3];
      wx.navigateToMiniProgram({
        appId: 'wx31b43acf6e51cff0',
        extraData: {
          apSsid: apSsid,
          apPassword: '12345678',
          hideApInfoInputs: true,
          guide: {
            android: guideSteps,
            ios: guideSteps
          },
          userData: null
        },
        envVersion: 'release',
        success: function (res) {
          console.log("调用配网小程序成功!");
        },
        fail: function (res) {
          console.error("调用配网小程序失败!");
        },
        complete: function (res) {},
      });
    }
  },

  onChangeField(event) {
    // event.detail 为当前输入的值
    console.log(event.detail);
    let inputType = event.currentTarget.dataset['inputType'];
    let device = this.data.device;
    device[inputType] = event.detail;
    this.setData({
      device: device
    });
  },

  validDevice(deviceId){
    return new Promise((resolve, reject) => {
      api.listRelationInfos(deviceId).then(res => {
        if (res && res.statusCode == 200) {
          if(res.data.length > 0){
            resolve(res.data[0].fromName);
          }else{
            resolve("");
          }
        } else {
          reject();
        }
      })
    })
     
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let deviceId = JSON.parse(options.deviceId);
    let isBind = options.isBind;
    this.setData({
      deviceId: deviceId,
      isBind : isBind == 'true' ? true : false
    });

    this.getDeviceDetail();
  },



  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})