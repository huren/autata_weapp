// pages/store/poder-device-formula/index.js
import Toast from '@vant/weapp/toast/toast';
const api = require('./../../../utils/request.js').default;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    deviceId: null,
    formulaId: null,
    formulas: [],
    pending:false,

    scroll1: {
      pagination: {
        page: 1,
        totalPage: 2,
        limit: 50,
        length: 50
      },
      empty: {
        img: './../../imgs/empty.png',
        text: '暂无配方数据'
      },
      refresh: {
        type: 'default',
        style: 'black',
        background: "#000"
      },
      loadmore: {
        type: 'default',
        icon: './../../imgs/loading.gif',
        background: '#FFF',
        title: {
          show: true,
          text: '加载中...',
          color: "#999"
        }
      }
    },
  },

  refresh1: function () {
    this.getDeviceFormulas('refresh', 1);
  },

  loadMore1: function () {
    console.log("page:" +this.data.scroll1.pagination.page );
    console.log("totalPage:" +this.data.scroll1.pagination.totalPage );
    if(this.data.scroll1.pagination.page >= this.data.scroll1.pagination.totalPage){
      return;
    }
    if(this.data.pending){
      return;
    }
    this.setData({
      pending:true
    })
    this.getDeviceFormulas('loadMore', this.data.scroll1.pagination.page + 1)
  },

  getDeviceFormulas(loadType, page) {
    let that = this;
    let deviceId = this.data.deviceId;
    let params = {};
    let subParams = {};
    subParams.formulaId = ['page' + page];
    params.method = "GET_FORMULA";
    params.params = subParams;
    params.timeout = 100000;
    
    api.getDeviceModel(deviceId, params).then(res => {
      that.setData({
        pending:false
      })
      if (res && res.statusCode == 200) {
        let formulas = res.data.formula;
        if (loadType == 'refresh') {
          let scroll = that.data.scroll1;
          scroll.pagination.page = page;
          if(formulas.length >= 50){
            scroll.pagination.totalPage = page + 1;
          }else{
            scroll.pagination.totalPage = page;
          }
         
          scroll.pagination.length = formulas.length;
          that.setData({
            formulas: formulas,
            scroll1: scroll
          });
        } else {
          let list = that.data.formulas;
          that.setData({
            formulas: list.concat(formulas)
          });
          console.log("----------------------------" + list.length);
          let scroll = that.data.scroll1;
          scroll.pagination.page = page;
          if (list.length >=  scroll.pagination.limit && scroll.pagination.page < 10) {
            scroll.pagination.totalPage = scroll.pagination.page + 1;
            scroll.pagination.length = that.data.formulas.length;
          } else {
            scroll.pagination.totalPage = scroll.pagination.page;
            scroll.pagination.length = that.data.formulas.length;
          }
          that.setData({
            scroll1: scroll
          });
        }
      } else {
        Toast.fail("数据获取失败!");
      }
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let deviceId = JSON.parse(options.deviceId);
    this.setData({
      deviceId: deviceId
    });
    this.getDeviceFormulas('refresh', this.data.scroll1.pagination.page);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})