// pages/store/device-setting/index.js

import Toast from '@vant/weapp/toast/toast';
import Dialog from '@vant/weapp/dialog/dialog';
const api = require('./../../../utils/request.js').default;
const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    webUserInfo: {},
    device: {},
    editDeviceName: false,
    tempDeviceName: '',
    modelUpGradeable: null,
    formulaUpGradeable: null,
    showSheet: false,
    modelList: [
      {
        index: 1,
        sName: 'model1', 
        id: null,
        name: '模式Ⅰ',
        disabled: true,
        color: null,
        step: 'STEP1'
      },
      {
        index: 2, 
        sName: 'model2', 
        id: null,
        name: '模式Ⅱ',
        disabled: true,
        color: null,
        step: 'STEP2'
      },
      {
        index: 3, 
        sName: 'model3', 
        id: null,
        name: '模式Ⅲ',
        disabled: true,
        color: null,
        step: 'STEP3'
      }
    ],
    formula: {},

    showFormulaSheet: false,
    formulaActions: [{name:'确定'},{name:'忽略'}],
    formulaDescription: '',
    updateFormulaData: null,

    showModelSheet: false,
    modelActions: [{name:'确定'},{name:'忽略'}],
    modelDescription: '',
    updateModelData: null,
    selectModelItem: null

  },

  // 设备模式
  onDeviceModel() {
    wx.navigateTo({
      url: '/pages/store/cooking-device-model/index?deviceId=' + JSON.stringify(this.data.device.id.id),
    })
  },

  // 设备配方
  onDeviceFormula() {
    wx.navigateTo({
      url: '/pages/store/poder-device-formula/index?deviceId=' + JSON.stringify(this.data.device.id.id),
    })
  },

  onDeviceConnect() {
    if (this.data.device && this.data.device.batchNum) {
      let apSsid = this.data.device.batchNum.length > 16 ? this.data.device.batchNum.substring(0, 16) : this.data.device.batchNum;
      let step1 = "按配网键进入小程序配网，该设备AP SSID：" + apSsid;
      let step2 = "确保手机已连接Wi-Fi";
      let step3 = "输入Wi-Fi密码后连接";
      let guideSteps = [step1, step2, step3];
      wx.navigateToMiniProgram({
        appId: 'wx31b43acf6e51cff0',
        extraData: {
          apSsid: apSsid,
          apPassword: '12345678',
          hideApInfoInputs: true,
          guide: {
            android: guideSteps,
            ios: guideSteps
          },
          userData: null
        },
        envVersion: 'release',
        success: function (res) {
          console.log("调用配网小程序成功!");
        },
        fail: function (res) {
          console.error("调用配网小程序失败!");
        },
        complete: function (res) {},
      });
    }
  },

  onDeviceReport() {
    const deviceId = this.data.device.id.id;
    const deviceName = this.data.device.name;
    const deviceType = this.data.device.type;
    wx.navigateTo({
      url: '/pages/store/device-report-repair/index?deviceId=' + deviceId + '&deviceName=' + deviceName + '&type=' + deviceType
    })
  },

  onChangeDeviceUsable({
    detail
  }) {
    
    Dialog.confirm({
        title: '警告',
        message: detail ? '是否禁用该设备?' : '是否启用该设备?',
      })
      .then(() => {
        // on confirm
        let device = this.data.device;
        device.additionalInfo = {}
        device.additionalInfo.enable = !detail;

        this.saveDevice(device);
      })
      .catch(() => {
        // on cancel
      });
  },

  resetDeviceData() {
    let pages = getCurrentPages();
    let beforePage = pages[pages.length - 2];
    beforePage.resetDevice&&beforePage.resetDevice(this.data.device);

    let earlyPage = pages[pages.length - 3];
    earlyPage.refreshData&&earlyPage.refreshData();
  },

  saveDevice(device) {
    Toast.loading({
      duration: 0,
      message: '设置中...'
    });

    let toastPrefix = device.additionalInfo.enable ? "启用" : "禁用";
    let params = device;
    api.saveDevice(params).then(res => {
      if (res && res.statusCode == 200) {
        console.log(res);
        Toast.success(toastPrefix + "设备成功!");
        this.setData({
          device: device
        })
        this.resetDeviceData();
      } else {
        console.error(res);
        Toast.fail(toastPrefix + "设备失败!");

        device.additionalInfo.enable = !device.additionalInfo.enable;
        this.setData({
          device: device
        })
      }
    })
  },

  unBindDevice() {
    let params = {};
    params.fromId = app.getStoreInfo().id.id;
    params.fromType = app.getStoreInfo().id.entityType;
    params.toId = this.data.device.id.id;
    params.toType = this.data.device.id.entityType;
    params.relationType = 'Contains';
    api.unBindStoreDevice(params).then(res => {
      if (res && res.statusCode == 200) {
        console.log(res);
        let pages = getCurrentPages(); // 当前页面
        let beforePage = pages[pages.length - 3]; // 前一个页面
        Toast({
          type: 'success',
          message: '已解绑设备!',
          onClose: () => {
            wx.navigateBack({
              delta: 2,
              success: function() {
                beforePage && beforePage.refreshData && beforePage.refreshData();
              }
            });
          },
        });
      } else {
        console.error(res);
        Toast.fail("设备解绑失败!");
      }
    })
  }, 

  saveDevice2(device) {
    Toast.loading({
      duration: 0,
      message: '保存中...'
    });

    let params = device;
    api.saveDevice(params).then(res => {
      if (res && res.statusCode == 200) {
        console.log(res);
        Toast.success("修改成功!");
        this.setData({
          device: device,
          editDeviceName: false,
          tempDeviceName: ''
        });
        this.resetDeviceData();
      } else {
        console.error(res);
        Toast.fail("修改失败!");
      }
    })
  },

  onChangeField(event) {
    console.log(event.detail);
    this.setData({
      tempDeviceName: event.detail
    });
  },

  onEditDeviceName() {
    this.setData({
      editDeviceName: true,
      tempDeviceName: this.data.device.label
    });
  },

  onSaveDeviceName() {
    let device = this.data.device;
    device.label = this.data.tempDeviceName;
    this.saveDevice2(device);
  },

  onCancelDeviceName() {
    this.setData({
      editDeviceName: false,
      tempDeviceName: this.data.device.label
    });
  },

  onModelUpdate() {
    if (this.data.modelUpGradeable) {
      // 模式更新
      this.setData({
        showSheet: true
      });
    } else {
      Toast("无新模式");      
    }
  },

  onCloseSheet() {
    this.setData({ showSheet: false });
  },

  onSelectSheet(event) {
    console.log(event.detail);
    let item = event.detail;

    let that = this;
    Toast.loading({
      duration: 0,
      message: '加载中...'
    });

    let params = {
      id: item.id
    };
    api.getModel(params).then(res => {
      if (res && res.statusCode == 200) {
        console.log(res);
        // 获取模式详情成功
        // 更新模式 
        Toast.clear();

        let message = "确定更新模式 \"" + res.data.label + "\"?";
        that.setData({ 
          showModelSheet: true,
          modelDescription: message,
          updateModelData: res.data,
          selectModelItem: item
        });
      } else {
        console.error(res);
        Toast.fail("模式加载失败!");
      }
    })
  },

  onCloseFormulaSheet() {
    this.setData({ 
      showFormulaSheet: false,
      formulaDescription: '',
      updateFormulaData: null
     });
  },

  onSelectFormulaSheet(event) {
    console.log(event.detail);
    let item = event.detail;
    if (item.name == '确定') {
      if (this.data.updateFormulaData) {
        let formulas = this.data.updateFormulaData.map(item=>{
          delete item.name;
          return item;
        });
        let totalPage = formulas && formulas.length > 0 ? Math.ceil(formulas.length / 50) : 0;
        this.updateFormula(formulas, 1, totalPage);
      }
    } else if (item.name == '忽略') {
      // 删除配方
      this.handleFormula();
    }
  },

  onCloseModelSheet() {
    this.setData({ 
      showModelSheet: false,
      modelDescription: '',
      updateModelData: null,
      selectModelItem: null
    });
  },

  onSelectModelSheet(event) {
    console.log(event.detail);
    let item = event.detail;
    if (item.name == '确定') {
      if (this.data.updateModelData && this.data.selectModelItem) {
        this.updateModel(this.data.selectModelItem, this.data.updateModelData);
      }
    } else if (item.name == '忽略') {
      // 删除模式
      if (this.data.selectModelItem) {
        this.handleModel(this.data.selectModelItem);
      }
    }
  },

  onFormulaUpdate() {
    if (this.data.formulaUpGradeable) {
      let that = this;
      Toast.loading({
        duration: 0,
        message: '加载中...'
      });
  
      let params = {
        id: this.data.formula
      };
      api.getModel(params).then(res => {
        if (res && res.statusCode == 200) {
          console.log(res);
          Toast.clear();

          let message = "确定更新配方组 \"" + res.data.name + "\"?";
          this.setData({
            showFormulaSheet: true,
            formulaDescription: message,
            updateFormulaData: res.data.additionalInfo.formulas
          });
        } else {
          console.error(res);
          Toast.fail("配方更新失败!");
        }
      })
    } else {
      Toast("无新配方");
    }
  },

  onPasswordUpdate() {
    Toast.loading({
      duration: 0,
      message: '更新中...'
    });
    // 口令更新
    const customerId = app.getWebUserInfo().customerId;
    const deviceId = this.data.device.id.id;
    api.devicePassword(customerId, deviceId, null).then(res => {
      if (res && res.statusCode == 200) {
        console.log(res);
        Toast.success("口令已更新!");
      } else {
        console.error(res);
        Toast.fail("口令更新失败!");
      }
    })
  },

  onUnBindStoreDevice() {
    Dialog.confirm({
      title: '警告',
      message: '是否解绑设备?',
    })
    .then(() => {
      // on confirm
      this.unBindDevice();
    })
    .catch(() => {
      // on cancel
    });
  },

  updateModel(item, model) {
    let that = this;
    Toast.loading({
      duration: 0,
      message: '更新中...'
    });

    let deviceId = that.data.device.id.id;
    let params2 = {};
    let subParams2 = {};
    subParams2.processGroup = item.index;
    subParams2.processGroupName = model.label;
    subParams2.steps = model.additionalInfo.steps.map(item=>{
      delete item.id;
      return item;
    })
    params2.method = "SET_PROCESS";
    params2.timeout = 10000;
    params2.params = subParams2;
    api.setDeviceModel(deviceId, params2).then(res => {
      if (res && res.statusCode == 200) {
        console.log(res);
        Toast.success("模式更新成功!");
        that.handleModel(item);
      } else {
        console.error(res);
        Toast.fail("模式更新失败!");
      }
    });
  }, 

  handleModel(item) {
    let modelList = this.data.modelList;
    modelList.forEach(model => {
      if (item.name === model.name) {
        model.id = null;
        model.disabled = true;
        model.color = null;
        
        item = model;
      }
    });
    this.setData({
      modelList: modelList
    });

    let pages = getCurrentPages();
    let beforePage = pages[pages.length - 2];
    beforePage.resetNewModels&&beforePage.resetNewModels(item.sName);
    this.showNewModelTag();
    this.deleteMode(item.step);
  },

  deleteMode(keys) {
    let deviceId = this.data.device.id.id;
    let params = {};
    params.keys = keys;
    api.deleteNewModel(deviceId, params).then(res => {
      if (res && res.statusCode == 200) {
        console.log(res);
      } else {
        console.log("删除更新的模式失败!");
      }
    });
  },

  updateFormula(formulas, pageIndex, totalPage) {
    let that = this;
    if (pageIndex == 1) {
      Toast.loading({
        duration: 0,
        message: '更新中...'
      });
    }  

    let subFormulas = formulas && formulas.length > 50 ? formulas.splice(0, 50) : formulas;

    // 更新配方
    let deviceId = that.data.device.id.id;
    let params2 = {};
    let subParams2 = {};
    //更新配方移除非必要参数
    subParams2.formula = subFormulas;
    subParams2.current = pageIndex;
    subParams2.total = totalPage;

    params2.method = "SET_FORMULA";
    params2.timeout = 100000;
    params2.params = subParams2;

    console.log("更新配方参数：" + JSON.stringify(params2));
    api.setDeviceModel(deviceId, params2).then(res => {
      if (res && res.statusCode == 200) {
        console.log(res);
        if (pageIndex >= totalPage) {
          Toast.success("配方更新成功!");
          that.handleFormula();
        } else {
          setTimeout(function () {
            that.updateFormula(formulas, pageIndex + 1, totalPage);
          }, 1000);
        }
      } else {
        console.error(res);
        Toast.fail("配方更新失败!");
      }
    });
  }, 

  handleFormula() {
    this.setData({
      formula: {},
      formulaUpGradeable: null
    });

    let pages = getCurrentPages();
    let beforePage = pages[pages.length - 2];
    beforePage.resetWDG&&beforePage.resetWDG();
    // 删除配方
    this.deleteFormula();
  },

  deleteFormula() {
    let deviceId = this.data.device.id.id;
    let deviceType = this.data.device.type;
    let params = {};
    if(deviceType == '果糖机'){
         params = {
           keys: 'WDG'
         }
      }else if(deviceType == '粉料机'){
         params = {
           keys: 'WDP'
         }
      }
    api.deleteNewModel(deviceId, params).then(res => {
      if (res && res.statusCode == 200) {
        console.log(res);
      } else {
        console.log("删除更新的配方失败!");
      }
    });
  },

  showNewModelTag() {
    let modelUpGradeable = false;
    
    let modelList = this.data.modelList;

    console.log("===" + JSON.stringify(modelList));
    modelList.forEach(model => {
      if (modelUpGradeable == false && !model.disabled) {
        modelUpGradeable = true;
      } 
    });

    this.setData({
      modelUpGradeable: modelUpGradeable
    });
  }, 

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      webUserInfo: app.getWebUserInfo()
    });
    
    let device = JSON.parse(options.device);
    let modelList = this.data.modelList;

    if (options.newModels) {
      let newModels = JSON.parse(options.newModels);
      Object.keys(newModels).forEach(key => {
        modelList.forEach(model => {
          if (key == model.sName) {
            model.id = newModels[key];
            model.disabled = false;
            model.color = 'red';
            model.name = model.name;
          } 
        });
      });

      // 设备配网成功的回调
      app.deviceConnectedCallback = data => {
        try {
          var device = data['referrerInfo']['extraData'];
          if (device) {
            Dialog.alert({
              title: '配网成功',
              message: JSON.stringify(device),
              theme: 'round-button',
            }).then(() => {
              // on close
            });
          } else {
            Toast('配网失败!');
          }
        } catch (error) {
          Toast('配网失败!');
        }
      }
    }

    if (options.wdg) {
      let wdg = JSON.parse(options.wdg);
      if (wdg) {
        this.setData({
          formula: wdg,
          formulaUpGradeable: true
        });
      }
    }

    // 默认开启设备
    if (!device.additionalInfo || !device.additionalInfo.enable) {
      device.additionalInfo = {};
      device.additionalInfo.enable = false;
    }
 
    this.setData({
      device: device,
      modelList: modelList
    });
    this.showNewModelTag();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})