// pages/store/poder-device/index.js
import Toast from '@vant/weapp/toast/toast';
import Dialog from '@vant/weapp/dialog/dialog';

const api = require('./../../../utils/request.js').default;
const url = require('./../../../utils/url.js').default;
const util = require("./../../../utils/util.js");
const app = getApp();
let webScoket = null;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    title: '粉料机/果糖机',
    deviceInfo: {},

    socketOpen: false,
    wsResult1: {},
    wsResult2: {},
    wdg: null,

    isOnLine: -1,// -1 未赋值，0 离线, 1 在线

    // 设备状态
    deviceStatus: {
      240: {
        name: "休眠",
        image: "../../../imgs/close.png"
      },
      241: {
        name: "待机",
        image: "../../../imgs/close.png"
      },
      242: {
        name: "出料",
        image: "../../../imgs/play.png"
      },
      245: {
        name: "故障停机",
        image: "../../../imgs/close.png"
      }
    },

    minHour: 0,
    maxHour: 23,
    minDate: new Date().getTime(),
    currentDate: null,
    formatCurrentDate: '',
    showModelPopup: false,
    showDelayModelPopup: false,
    showTimePopup: false,
    temp: '1',
    delayTemp: '1',
    delayTime: null,
    formatDelayTime: '',
    enableDelay: false,
    tempRange: {
      min: 1,
      max: 40
    }
  },

  onInputTime(event) {
    this.setData({
      currentDate: event.detail,
      formatCurrentDate: util.formatDate2(event.detail)
    });
  },

  onConfirmTime() {
    this.onCloseTimePopup();
  },

  onCancelTime() {
    this.onCloseTimePopup();
  },

  onShowModelPopup() {
    if (this.data.isOnLine != 1) {
      Toast.fail("离线不可操作");
      return;
    }
    if (this.data.wsResult1.deviceStatusValue == "240") {
      Toast.fail("休眠不可操作");
      return;
    }
    if (this.data.wsResult1.deviceStatusValue == "241") {
      // 开启模式
      this.setData({
        showModelPopup: true
      });
    } else {
      // 关闭模式
      Toast.loading({
        duration: 0,
        message: '关闭预热...'
      });
  
      let deviceId = this.data.deviceInfo.id.id;
      let params = {};
      let subParams = {};
      subParams.workMode = this.data.wsResult1.workModeValue;
      params.method = "CLOSE_PROCESS";
      params.params = subParams;
      params.timeout = 100000;
      api.setDelayDeviceModel(deviceId, params).then(res => {
        if (res && res.statusCode == 200) {
          Toast.fail("预热已关闭!");
        } else {
          Toast.fail("预热关闭失败!");
        }
      });
    }
  },

  onShowDelayModelPopup() {
    if (this.data.enableDelay) {
      Dialog.confirm({
        title: '警告',
        message: '确定取消预约？',
      })
      .then(() => {
        // on confirm
        Toast.loading({
          duration: 0,
          message: '取消预约...'
        });
    
        // 取消预约
        let deviceId = this.data.deviceInfo.id.id;
        let params = {};
        let subParams = {};
        params.method = "HOT_DEVICE";
        params.params = subParams;
        subParams.preheatFlag = 'ON';
        api.deleteDelayDeviceModel(deviceId, params).then(res => {
          if (res && res.statusCode == 200) {
            Toast.success("预约已取消!");

            this.setData({
              // currentDate: detail.time,
              formatCurrentDate: '',
              // delayTime: detail.time,
              formatDelayTime: '',
              enableDelay: false
            });

          } else {
            Toast.fail('预约取消失败!');
          }
        });
      })
      .catch(() => {
        // on cancel
      });
    } else {
      this.setData({
        showDelayModelPopup: true
      });
    }
  },

  onShowTimePopup() {
    this.setData({
      showTimePopup: true
    });
  },

  onCloseModelPopup() {
    this.setData({
      showModelPopup: false
    });
  },

  onCloseDelayModelPopup() {
    this.setData({
      showDelayModelPopup: false
    });
  },

  onCloseTimePopup() {
    this.setData({
      showTimePopup: false
    });
  },

  onClickModelCancel() {
    this.onCloseModelPopup();
  },

  onClickModelOk() {
    this.onCloseModelPopup();

    Dialog.confirm({
      title: '警告',
      message: '确定开启预热？',
    })
    .then(() => {
      // on confirm
      Toast.loading({
        duration: 0,
        message: '开启预热...'
      });
  
      // 预热
      let deviceId = this.data.deviceInfo.id.id;
      let params = {};
      let subParams = {};
      subParams.preheatFlag = "ON";
      subParams.tempeture = this.data.temp + '';
      params.method = "HOT_DEVICE";
      params.params = subParams;
      params.timeout = 100000;
      api.setDeviceHot(deviceId, params).then(res => {
        if (res && res.statusCode == 200) {
          Toast.fail("已开启预热!");
        } else {
          Toast.fail("预热失败!");
        }
      });
    })
    .catch(() => {
      // on cancel
    });
  },

  onClickDelayModelCancel() {
    this.onCloseDelayModelPopup();
  },

  onClickDelayModelOk() {
    this.onCloseDelayModelPopup();

    Dialog.confirm({
      title: '警告',
      message: '确定开启预热预约？',
    })
    .then(() => {
      // on confirm
      Toast.loading({
        duration: 0,
        message: '预热预约...'
      });
  
      // 启动预约模式
      let deviceId = this.data.deviceInfo.id.id;
      let params = {};
      let subParams = {};
      subParams.preheatFlag = "ON";
      subParams.tempeture = this.data.delayTemp + '';
      params.method = "HOT_DEVICE";
      params.params = subParams;
      // params.timeout = 100000;
      params.time = this.data.currentDate;
      debugger
      
      api.setDelayDeviceModel(deviceId, params).then(res => {
        if (res && res.statusCode == 200) {
          Toast.success("预热预约成功!");
        } else {
          let msg = res && res.data.errorCode == 31 ? '预约已预约!' : '预热预约失败!';
          Toast.fail(msg);
        }
      });
    })
    .catch(() => {
      // on cancel
    });
  },

  onChangeTemp(event) {
    let temp = event.detail;
    if (event.detail > this.data.tempRange.max) {
      Toast.fail('温度不能超过' + this.data.tempRange.max + '℃');
      temp = this.data.tempRange.max;
    }
    this.setData({
      temp: temp,
    });
  },

  onChangeDelayTemp(event) {
    let temp = event.detail;
    if (event.detail > this.data.tempRange.max) {
      Toast.fail('温度不能超过' + this.data.tempRange.max + '℃');
      temp = this.data.tempRange.max;
    }
    this.setData({
      delayTemp: temp
    });
  },

  onSetting() {
    let device = JSON.stringify(this.data.deviceInfo);
    let wdg = JSON.stringify(this.data.wdg);
    wx.navigateTo({
      url: '/pages/store/device-setting/index?device=' + device + '&wdg=' + wdg,
    })
  },

  resetDevice(device) {
    let deviceInfo = this.data.deviceInfo;
    deviceInfo.deviceLabel = device.deviceLabel;
    deviceInfo.additionalInfo = device.additionalInfo;
    this.setData({deviceInfo: deviceInfo});
  },

  resetWDG() {
    this.setData({
      wdg: null
    });
  },

  connectWebSocket() {
    if (!this.data.deviceInfo) {
      return;
    }

    let that = this;
    webScoket = wx.connectSocket({
      url: url.wsUrl + '?token=' + app.getToken(),
      timeout: 60000,
      success: function (res) {
        console.log("WebSocket连接成功! res = " + JSON.stringify(res));
      },
      fail: function (res) {
        console.error("WebSocket连接失败! res = " + JSON.stringify(res));
      }
    });

    webScoket.onOpen(function (res) {
      console.info("WebSocket开启! res = " + JSON.stringify(res));
      that.setData({
        socketOpen: true
      });
      // 发送订阅消息
      that.sendWebSocketMessage();
    });

    webScoket.onMessage(function (res) {
      console.log("WebSocket接收服务器消息! res = " + JSON.stringify(res));
      if (res && res.data) {
        let result = JSON.parse(res.data);
        if (result.errorCode == 0 && result.data) {
          if (that.data.deviceInfo.index << 1 == result.subscriptionId) {
            // 当前模式信息
            let wsResult = result.data;
            console.log(wsResult);
            // 格式化结果
            try {
              wsResult.deviceStatusValue = wsResult.deviceStatus && wsResult.deviceStatus[0][1];
              wsResult.gramsWeightValue = wsResult.gramsWeight && wsResult.gramsWeight[0][1];
              wsResult.tempeMaxValue = wsResult.tempeMax && wsResult.tempeMax[0][1];
              wsResult.tempetureValue = wsResult.tempeture && wsResult.tempeture[0][1];
              wsResult.errorCodeValue = wsResult.errorCode && wsResult.errorCode[0][1];

              wsResult.deviceStatusImage = that.data.deviceStatus[wsResult.deviceStatusValue].image;
              wsResult.deviceStatusName = that.data.deviceStatus[wsResult.deviceStatusValue].name;

            } catch (error) {
              console.error("格式化设备信息出错: " + JSON.stringify(error));
            } finally {
              console.log(wsResult);
            }

            that.setData({
              wsResult1: wsResult
            });     
            
          } else if (that.data.deviceInfo.index == result.subscriptionId) {
            let wsResult = result.data;
            console.log(wsResult);
            that.setData({
              wsResult2: wsResult
            });

             //设备在线不在线  
             if(wsResult.active && wsResult.active.length > 0) {
              try {
                 let onLine = JSON.parse(wsResult.active[0][1]);
                 that.setData({
                  isOnLine: onLine ? 1 : 0
                 });
              } catch (error) {
                that.setData({
                  isOnLine: -1
                });
              }
            };

            if (wsResult.hotSubScribe) {
              let detail = JSON.parse(wsResult.hotSubScribe[0][1]);
              that.setData({
                // currentDate: detail.time,
                formatCurrentDate: util.formatDate2(detail.time),
                // delayTime: detail.time,
                formatDelayTime: util.formatDate2(detail.time),
                enableDelay: true
              });
            }

            if (wsResult.WDG) {
              let wdg = wsResult.WDG[0][1];
              that.setData({
                wdg: wdg
              });
            }
            
          }
        }
      }
    });

    webScoket.onError(function (res) {
      console.error("WebSocket错误! res = " + JSON.stringify(res));
    });

  },

  sendWebSocketMessage() {
    if (this.data.socketOpen && webScoket) {
      let item = this.data.deviceInfo;
      let msg = {};
      let attrSubCmds = [];
      let tsSubCmds = [];
      let attrSubCmd = {};
      let tsSubCmd = {};

      attrSubCmd.cmdId = item.index;
      attrSubCmd.entityId = item.id.id;
      attrSubCmd.entityType = item.id.entityType;
      attrSubCmd.scope = "SERVER_SCOPE";
      attrSubCmds.push(attrSubCmd);
      msg.attrSubCmds = attrSubCmds;

      tsSubCmd.cmdId = item.index << 1;
      tsSubCmd.entityId = item.id.id;
      tsSubCmd.entityType = item.id.entityType;
      tsSubCmd.scope = "LATEST_TELEMETRY";
      tsSubCmds.push(tsSubCmd);
      msg.tsSubCmds = tsSubCmds;

      console.log('WebSocket发送给服务端的消息! msg = ' + JSON.stringify(msg));
      webScoket.send({
        data: JSON.stringify(msg)
      })

    } else {
      console.error('WebSocket已关闭! 不能发送消息');
    }
  },

  closeWebSocket() {
    if (this.data.socketOpen && webScoket) {
      webScoket.close(function (res) {
        console.log('WebSocket已关闭！')
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let deviceInfo = JSON.parse(options.deviceInfo);
    deviceInfo.formatProductionDate = deviceInfo.productionDate ? util.formatDate(deviceInfo.productionDate) : null;
    console.log(deviceInfo);
    this.setData({
      title: deviceInfo.type + '(' + (deviceInfo.label || deviceInfo.batchNum || deviceInfo.name) + ')',
      deviceInfo: deviceInfo
    });
    this.connectWebSocket();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.setKeepScreenOn({
      keepScreenOn: true,
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    this.closeWebSocket();
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})