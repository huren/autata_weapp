// pages/store/device-report-repair/index.js

import Toast from '@vant/weapp/toast/toast';
const md5 = require('./../../../utils/md5.js');
const qiniuUploader = require("./../../../utils/qiniuUploader.js");
const api = require('./../../../utils/request.js').default;
const url = require('./../../../utils/url.js').default;
const app = getApp();
// let uploadTask = null;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    ossToken: null,
    fileList: [],
    reportDetail: {
      additionalInfo:{
        
      }
    },
    // 此属性在qiniuUploader.upload()中被赋值，用于中断上传
    cancelTask: function () { }
  },

  // 初始化七牛云相关配置
  initQiniu(data) {
    var options = {
      // bucket所在区域，这里是华北区。ECN, SCN, NCN, NA, ASG，分别对应七牛云的：华东，华南，华北，北美，新加坡 5 个区域
      region: 'SCN',
      uptoken: data.ossToken,
      domain: data.cdnUrl,
      shouldUseQiniuFileName: false
    };
    // 将七牛云相关配置初始化进本sdk
    qiniuUploader.init(options);
  },

  afterRead(event) {
    let that = this;
    const {
      file
    } = event.detail;
    file.name = md5.hexMD5(Math.random().toString(36).substr(2) + new Date().getTime()) // 文件名
    file.status = 'uploading';
    file.message = '上传中';

    // 向七牛云上传
    qiniuUploader.upload(file.path, (res) => {
        // res.fileName = fileName;
        console.log(res);

        const { fileList = [] } = this.data;
        fileList.push({ ...file, url: res.fileURL, status: 'done', message: '上传成功' });
        this.setData({ fileList });
        
        console.log('file url is: ' + res.fileURL);
        console.log(this.data.fileList);
      }, (error) => {
        console.error('error: ' + JSON.stringify(error));

        const { fileList = [] } = this.data;
        fileList.push({ ...file, url: null, status: 'failed', message: '上传失败' });
        this.setData({ fileList });
      },
      {
        region: 'SCN',
        uptoken: that.data.ossToken,
        key: file.name
      },
      (progress) => {
        // that.setData({
        //   'messageFileProgress': progress
        // });
        console.log('上传进度', progress.progress);
        console.log('已经上传的数据长度', progress.totalBytesSent);
        console.log('预期需要上传的数据总长度', progress.totalBytesExpectedToSend);
      }, cancelTask => that.setData({
        cancelTask
      })
    );
  },

  onDeleteFile: function (event) {
    const { fileList = [] } = this.data;
    fileList.splice(event.detail.index);
    this.setData({ fileList });

    // this.data.cancelTask();
  },

  getOssToken() {
    let params = {};
    api.getOssToken(params).then(res => {
      console.log(res)
      if (res && res.statusCode == 200) {
        this.setData({
          ossToken: res.data
        });
        this.initQiniu({
          ossToken: this.data.ossToken,
          cdnUrl: url.cdnUrl
        });
      } else {
        Toast.fail("获取oss token失败!");
      }
    })
  },

  onChangeField(event) {
    // event.detail 为当前输入的值
    console.log(event.detail);
    let inputType = event.currentTarget.dataset['inputType'];
    let reportDetail = this.data.reportDetail;
    reportDetail[inputType] = event.detail;
    this.setData({
      reportDetail: reportDetail
    });
  },

  onReportRepair() {
    const { reportDetail = {} } = this.data;
    if (!reportDetail.contact || reportDetail.contact.replace(/[ ]/g, '').length == 0) {
      Toast('反馈人不能为空!');
      return;
    }
    if (!reportDetail.phone || reportDetail.phone.replace(/[ ]/g, '').length == 0) {
      Toast('手机号不能为空!');
      return;
    }
    if (!reportDetail.title || reportDetail.title.replace(/[ ]/g, '').length == 0) {
      Toast('标题不能为空!');
      return;
    }
    if (!reportDetail.content || reportDetail.content.replace(/[ ]/g, '').length == 0) {
      Toast('故障描述不能为空!');
      return;
    }
    reportDetail.additionalInfo.images = [];
    this.data.fileList.forEach(item => {
      reportDetail.additionalInfo.images.push(item.url);
    });
    
    Toast.loading({
      duration: 0,
      message: '提交中...'
    });

    api.reportRepair(app.getWebUserInfo().customerId, reportDetail).then(res => {
      if (res && res.statusCode == 200) {
        Toast({
          type: 'success',
          message: '报修信息已提交!',
          onClose: () => {
            wx.navigateBack();
          },
        });
      } else {
        console.error(res);
        Toast.fail("报修信息提交失败!");
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const deviceId = options.deviceId;
    const deviceName = options.deviceName;
    const deviceType = options.type;
    const reportDetail = this.data.reportDetail;
    reportDetail.assetName = app.getStoreInfo().name;
    reportDetail.deviceId = deviceId;
    reportDetail.deviceName = deviceName;
    reportDetail.additionalInfo.deviceType = deviceType;
    this.setData({
      reportDetail
    });
    this.getOssToken();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})