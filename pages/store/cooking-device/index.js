// pages/store/cooking-device/index.js
import Toast from '@vant/weapp/toast/toast';
import Dialog from '@vant/weapp/dialog/dialog';
const api = require('./../../../utils/request.js').default;
const url = require('./../../../utils/url.js').default;
const util = require("./../../../utils/util.js");
const app = getApp();
let webScoket = null;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    title: '煮料机器人',
    deviceInfo: {},

    socketOpen: false,
    wsResult1: {},
    steps: [],

    isOnLine: -1,// -1 未赋值，0 离线, 1 在线

    // 设备状态
    deviceStatus: {
      240: {
        name: "休眠",
        image: "../../../imgs/close.png"
      },
      241: {
        name: "待机",
        image: "../../../imgs/close.png"
      },
      242: {
        name: "自动制作",
        image: "../../../imgs/play.png"
      },
      243: {
        name: "手动制作",
        image: "../../../imgs/play.png"
      },
      244:  {
        name: "制作完成",
        image: "../../../imgs/play.png"
      },
      245: {
        name: "故障停机",
        image: "../../../imgs/close.png"
      },
    },

    // 工作模式
    workMode: {
      0: "手动模式",
      1: "模式Ⅰ",
      2: "模式Ⅱ",
      3: "模式Ⅲ"
    },

    // 工序
    funName: {
      0: "空闲",
      1: "进水",
      2: "煮水",
      3: "煮料",
      4: "焖料",
      5: "保温",
      6: "蜜料",
      7: "洗料",
      8: "停止",
      9: "排水",
      16:"等待加料",
      17:"等待加糖"
    },

    // 搅拌
    stir: {
      0: {
        "name": "不搅拌",
        "image": "../../../imgs/none_stir.png"
      },
      1: {
        "name": "单向搅拌",
        "image": "../../../imgs/one_way_stir.png"
      },
      2: {
        "name": "双向搅拌",
        "image": "../../../imgs/two_way_stir.png"
      }
    },

    // 火力
    fire: {
      0: {
        "name": "无",
        "image": "../../../imgs/none_fire.png"
      },
      1: {
        "name": "小火",
        "image": "../../../imgs/small_fire.png"
      },
      2: {
        "name": "中火",
        "image": "../../../imgs/middle_fire.png"
      },
      3: {
        "name": "大火",
        "image": "../../../imgs/big_fire.png"
      }
    },

    // 恒温
    constTemp: {
      1: {
        "name": "恒温",
        "image": "../../../imgs/on_const_temp.png"
      },
      0: {
        "name": "不恒温",
        "image": "../../../imgs/off_const_temp.png"
      }
    },

    // 液位
    waterLevel: {
      1: "3L",
      2: "9L",
      3: "12L"
    },

    waterTempTop: 101,
    stoveTempTop: 101,
    waterTempView: {
      width: 10,
      height: 0,
      redLinePaddingTop: 0, 
      redTextPaddingTop: 0
    },
    stoveTempView: {
      width: 10,
      height: 0
    },

    minHour: 0,
    maxHour: 23,
    minDate: new Date().getTime(),
    // maxDate: new Date(2019, 10, 1).getTime(),
    currentDate: null,
    formatCurrentDate: '',
    showModelPopup: false,
    showDelayModelPopup: false,
    showTimePopup: false,
    modelRadio: '1',
    waterRadio: '1',
    delayModelRadio: '1',
    delayWaterRadio: '1',
    delayTime: null,
    formatDelayTime: '',
    delayModel: null,
    newModels: {}
  },

  onShowModelStep() {
    let currentStep = this.data.wsResult1.currentStepValue;
    let steps = this.data.steps;
    if (steps && steps.length > 0) {
      wx.navigateTo({
        url: '/pages/store/cooking-device-model-step/index?currentStep=' + JSON.stringify(currentStep) + '&steps=' + JSON.stringify(steps),
      })
    }
  },

  onInputTime(event) {
    this.setData({
      currentDate: event.detail,
      formatCurrentDate: util.formatDate2(event.detail)
    });
  },

  onConfirmTime() {
    this.onCloseTimePopup();
  },

  onCancelTime() {
    this.onCloseTimePopup();
  },

  onShowModelPopup() {
    if (this.data.isOnLine != 1) {
      Toast.fail("离线不可操作");
      return;
    }
    if (this.data.wsResult1.deviceStatusValue == "240") {
      Toast.fail("休眠不可操作");
      return;
    }
    if (this.data.wsResult1.deviceStatusValue == "243") {
      Toast.fail("手动模式下不可操作");
      return;
    }
    if (this.data.wsResult1.deviceStatusValue == "241") {
      // 开启模式
      this.setData({
        showModelPopup: true
      });
    } else {
      // 关闭模式
      Dialog.confirm({
        title: '警告',
        message: '确定关闭该模式吗？',
      })
      .then(() => {
        // on confirm
        Toast.loading({
          duration: 0,
          message: '关闭模式...'
        });
        let deviceId = this.data.deviceInfo.id.id;
        let params = {};
        let subParams = {};
        subParams.workMode = this.data.wsResult1.workModeValue;
        params.method = "CLOSE_PROCESS";
        params.params = subParams;
        params.timeout = 100000;
        api.setDeviceModel(deviceId, params).then(res => {
          if (res && res.statusCode == 200) {
            Toast.fail("模式已关闭!");
          } else {
            Toast.fail("模式关闭失败!");
          }
        });
      })
      .catch(() => {
        // on cancel
      });
    }
  },

  onShowDelayModelPopup() {
    if (this.data.wsResult1.deviceStatusValue == "243") {
      Toast.fail("手动模式下不可操作");
      return;
    }
    if (this.data.delayModel && this.data.delayModel.process) {
      Dialog.confirm({
        title: '警告',
        message: '确定取消该预约吗？',
      })
      .then(() => {
        // 取消预约
        Toast.loading({
          duration: 0,
          message: '取消预约...'
        });

        let deviceId = this.data.deviceInfo.id.id;
        let params = {};
        let subParams = {};
        params.method = "OPEN_PROCESS";
        params.params = subParams;
        subParams.workMode = this.data.delayModel.id;
        api.deleteDelayDeviceModel(deviceId, params).then(res => {
          if (res && res.statusCode == 200) {
            Toast.success("预约已取消!");

            this.setData({
              // currentDate: process.time,
              formatCurrentDate: '',
              // delayTime: process.time,
              formatDelayTime: '',
              delayModel: null
            });

          } else {
            Toast.fail('预约取消失败!');
          }
        });
      })
      .catch(() => {
        // on cancel
      });

    } else {
      this.setData({
        showDelayModelPopup: true
      });
    }
  },

  onShowTimePopup() {
    this.setData({
      showTimePopup: true
    });
  },

  onCloseModelPopup() {
    this.setData({
      showModelPopup: false
    });
  },

  onCloseDelayModelPopup() {
    this.setData({
      showDelayModelPopup: false
    });
  },

  onCloseTimePopup() {
    this.setData({
      showTimePopup: false
    });
  },

  onClickModelCancel() {
    this.onCloseModelPopup();
  },

  onClickModelOk() {
    this.onCloseModelPopup();
    let that = this;

    Dialog.confirm({
      title: '警告',
      message: '确定启动该模式吗？',
    })
    .then(() => {
      // on confirm
      Toast.loading({
        duration: 0,
        message: '启动模式...'
      });

      // 先获取模式详情,再启动模式
      
      this.getModelFromDevice(this.data.modelRadio, function(code, steps) {
        
        if (code == 1) {
          // 启动模式
          let deviceId = that.data.deviceInfo.id.id;
          let params = {};
          let subParams = {};
          subParams.level = that.data.waterRadio;
          subParams.workMode = that.data.modelRadio;
          params.method = "OPEN_PROCESS";
          params.params = subParams;
          params.timeout = 100000;
          api.setDeviceModel(deviceId, params).then(res => {
            if (res && res.statusCode == 200) {
              Toast.success("模式已启动!");
              
              // 设置工序
              that.setData({
                steps: steps
              });

              // 设置当前工序信息, 默认第0步为当前工序
              let wsResult = that.data.wsResult1;
              if (that.data.steps.length >= 1) {
                wsResult.currentStepName = that.data.steps[0].stepName;
                that.setData({
                  wsResult1: wsResult
                });
              }
              
            } else {
              Toast.fail("模式启动失败!");
            }
          });
        } else {
          Toast.fail('模式启动失败!');
        }
      })

    })
    .catch(() => {
      // on cancel
    });
  },

  onClickDelayModelCancel() {
    this.onCloseDelayModelPopup();
  },

  onClickDelayModelOk() {
    this.onCloseDelayModelPopup();

    Dialog.confirm({
      title: '警告',
      message: '确定开启模式预约吗？',
    })
    .then(() => {
      // on confirm
      Toast.loading({
        duration: 0,
        message: '模式预约...'
      });

      // 启动预约模式
      let deviceId = this.data.deviceInfo.id.id;
      let params = {};
      let subParams = {};
      subParams.level = this.data.delayWaterRadio;
      subParams.workMode = this.data.delayModelRadio;
      params.method = "OPEN_PROCESS";
      params.params = subParams;
      // params.timeout = 100000;
      params.time = this.data.currentDate;
      api.setDelayDeviceModel(deviceId, params).then(res => {
        if (res && res.statusCode == 200) {
          Toast.success("模式预约成功!");
        } else {
          let msg = res && res.data.errorCode == 31 ? '该模式已预约!' : '模式预约失败!';
          Toast.fail(msg);
        }
      });
    })
    .catch(() => {
      // on cancel
    });
  },

  onChangeModel(event) {
    this.setData({
      modelRadio: event.detail,
    });
  },

  onChangeWater(event) {
    this.setData({
      waterRadio: event.detail,
    });
  },

  onChangeDelayModel(event) {
    this.setData({
      delayModelRadio: event.detail,
    });
  },

  onChangeDelayWater(event) {
    this.setData({
      delayWaterRadio: event.detail,
    });
  },

  onSetting() {
    let device = JSON.stringify(this.data.deviceInfo);
    let newModels = JSON.stringify(this.data.newModels);
    wx.navigateTo({
      url: '/pages/store/device-setting/index?device=' + device + '&newModels=' + newModels,
    })
  },

  resetDevice(device) {
    let deviceInfo = this.data.deviceInfo;
    deviceInfo.deviceLabel = device.deviceLabel;
    deviceInfo.additionalInfo = device.additionalInfo;
    this.setData({deviceInfo: deviceInfo});
  },

  resetNewModels(model) {
    let newModels = this.data.newModels;
    delete newModels[model];
    this.setData({
      newModels: newModels
    });
  },

  forbidMove(e){return;},   
  connectWebSocket() {
    if (!this.data.deviceInfo) {
      return;
    }

    let that = this;
    webScoket = wx.connectSocket({
      url: url.wsUrl + '?token=' + app.getToken(),
      timeout: 60000,
      success: function (res) {
        console.log("WebSocket连接成功! res = " + JSON.stringify(res));
      },
      fail: function (res) {
        console.error("WebSocket连接失败! res = " + JSON.stringify(res));
      }
    });

    webScoket.onOpen(function (res) {
      console.info("WebSocket开启! res = " + JSON.stringify(res));
      that.setData({
        socketOpen: true
      });
      // 发送订阅消息
      that.sendWebSocketMessage();
    });

    webScoket.onMessage(function (res) {
      console.log("WebSocket接收服务器消息! res = " + JSON.stringify(res));
      if (res && res.data) {
        let result = JSON.parse(res.data);
        if (result.errorCode == 0 && result.data) {
          if (that.data.deviceInfo.index << 1 == result.subscriptionId) {
            // 当前模式信息
            let wsResult = result.data;
            console.log(wsResult);
            // 格式化结果
            try {
              wsResult.constantTempFlagValue = wsResult.constantTempFlag && wsResult.constantTempFlag[0][1];
              wsResult.setWaterLevelValue = wsResult.setWaterLevel && wsResult.setWaterLevel[0][1] != '0' ? ('液位' + wsResult.setWaterLevel[0][1]) : null;
              wsResult.currentStepValue = wsResult.currentStep && wsResult.currentStep[0][1] - 1;
              wsResult.deviceStatusValue = wsResult.deviceStatus && wsResult.deviceStatus[0][1];
              wsResult.errorCodeValue = wsResult.errorCode && wsResult.errorCode[0][1];
              wsResult.funNameValue = wsResult.funName && wsResult.funName[0][1];
              if(wsResult.funNameValue == "16"){
                wsResult.funNameExpandValue = "等待加料"
              }else if(wsResult.funNameValue == "17"){
                wsResult.funNameExpandValue = "等待加糖"
              }else{
                wsResult.funNameExpandValue = "";
              }
              wsResult.powerValue = wsResult.power && wsResult.power[0][1];
              wsResult.stirValue = wsResult.stir && wsResult.stir[0][1];
              wsResult.tempetureHeaterValue = wsResult.tempetureHeater && wsResult.tempetureHeater[0][1];
              wsResult.tempetureSetValue = wsResult.tempetureSet && wsResult.tempetureSet[0][1];
              wsResult.tempetureWaterValue = wsResult.tempetureWater && wsResult.tempetureWater[0][1];
              wsResult.timeRemainValue = util.formatSeconds(wsResult.timeRemain && wsResult.timeRemain[0][1]);
              wsResult.timeTotalValue = util.formatSeconds(wsResult.timeTotal && wsResult.timeTotal[0][1]);
              wsResult.timeWorkValue = util.formatSeconds(wsResult.timeWork && wsResult.timeWork[0][1]);
              wsResult.totalStepValue = wsResult.totalStep && wsResult.totalStep[0][1];
              // wsResult.userValue = wsResult.user[0][1];
              wsResult.workModeValue = wsResult.workMode && wsResult.workMode[0][1];
              wsResult.deviceStatusImage = that.data.deviceStatus[wsResult.deviceStatusValue].image;
              wsResult.deviceStatusName = that.data.deviceStatus[wsResult.deviceStatusValue].name;
              if (wsResult.deviceStatusValue == "240" || wsResult.deviceStatusValue == "241" || wsResult.deviceStatusValue == "245") {
                wsResult.deviceStatusLabel = "";
                //待机清空缓存步骤
                if(wsResult.deviceStatusValue == "241"){ 
                  that.setData({
                    steps: []
                  });   
                }
              } else {
                wsResult.deviceStatusLabel = that.data.workMode[wsResult.workModeValue];
                if (wsResult.setWaterLevelValue) {
                  wsResult.deviceStatusLabel = that.data.workMode[wsResult.workModeValue] + "(" + wsResult.setWaterLevelValue + ")";
                }
              }
            } catch (error) {
              console.error("格式化设备信息出错: " + JSON.stringify(error));
            } finally {
              console.log(wsResult);
            }

            that.setData({
              wsResult1: wsResult
            });     
            
            // 设置温度计参数
            // 水温最大120, 炉温最大200
            let waterTempFactor = 101 / 120;
            let stoveTempFactor = 101 / 200;
            let tempetureWaterValue = wsResult.tempetureWaterValue > 120 ? 120 : wsResult.tempetureWaterValue;
            let tempetureHeaterValue = wsResult.tempetureHeaterValue > 200 ? 200 : wsResult.tempetureHeaterValue;
            let tempetureSetValue = wsResult.tempetureSetValue > 120 ? 120 : wsResult.tempetureSetValue;
            let waterTempTop = parseInt(waterTempFactor * (120 - tempetureWaterValue));
            let stoveTempTop = parseInt(stoveTempFactor * (200 - tempetureHeaterValue));
            let waterTempView = {
              width: 10,
              height: parseInt(waterTempFactor * tempetureWaterValue),
              redLinePaddingTop: parseInt(waterTempFactor * ((120 - tempetureSetValue) <= 0 ? 0 : (120 - tempetureSetValue))), 
              redTextPaddingTop: parseInt(waterTempFactor * ((120 - tempetureSetValue - 14) <= 3 ? 3 : (120 - tempetureSetValue - 14)))
            };
            let stoveTempView = {
              width: 10,
              height: parseInt(stoveTempFactor * tempetureHeaterValue)
            }
            that.setData({
              waterTempTop,
              stoveTempTop,
              waterTempView,
              stoveTempView
            });

            // 获取当前模式详情, 从设备获取
            if(that.data.steps == null || that.data.steps.length == 0){
              that.getModelFromDevice(wsResult.workModeValue);
            }
            
          } else if (that.data.deviceInfo.index == result.subscriptionId) {
            let wsResult = result.data;
            console.log("=============" + JSON.stringify(wsResult));

            let newModels = that.data.newModels;
            if (wsResult.STEP1) {
              newModels.model1 = wsResult.STEP1[0][1];
            }
            if (wsResult.STEP2) {
              newModels.model2 = wsResult.STEP2[0][1];
            }
            if (wsResult.STEP3) {
              newModels.model3 = wsResult.STEP3[0][1];
            }

            that.setData({
              newModels: newModels
            });

            //设备在线不在线  
            if(wsResult.active && wsResult.active.length > 0) {
              try {
                 let onLine = JSON.parse(wsResult.active[0][1]);
                 that.setData({
                   isOnLine: onLine ? 1 : 0
                 });
              } catch (error) {
                that.setData({
                  isOnLine: -1
                });
              }
            };
            //设备预约  
            let delayModel = {};
            if (wsResult.modeSubScribe_1) {
              let process = JSON.parse(wsResult.modeSubScribe_1[0][1]);
              delayModel.id = 1;
              delayModel.modeStr = '模式Ⅰ';
              delayModel.process = process;
            }  
            
            if (wsResult.modeSubScribe_2) {
              let process = JSON.parse(wsResult.modeSubScribe_2[0][1]);
              if (!delayModel.process || delayModel.process.time > process.time) {
                delayModel.id = 2;
                delayModel.modeStr = '模式Ⅱ';
                delayModel.process = process;
              }
            } 
            
            if (wsResult.modeSubScribe_3) {
              let process = JSON.parse(wsResult.modeSubScribe_3[0][1]);
              if (!delayModel.process || delayModel.process.time > process.time) {
                delayModel.id = 3;
                delayModel.modeStr = '模式Ⅲ';
                delayModel.process = process;
              }
            }

            if (delayModel && delayModel.process) {
              that.setData({
                // currentDate: delayModel.process.time,
                formatCurrentDate: util.formatDate2(delayModel.process.time),
                // delayTime: delayModel.process.time,
                formatDelayTime: delayModel.modeStr + '(' + util.formatDate2(delayModel.process.time) + ')',
                delayModel: delayModel
              });
            } 
          }
        }
      }
    });

    webScoket.onError(function (res) {
      console.error("WebSocket错误! res = " + JSON.stringify(res));
    });

  },

  sendWebSocketMessage() {
    if (this.data.socketOpen && webScoket) {
      let item = this.data.deviceInfo;
      let msg = {};
      let attrSubCmds = [];
      let tsSubCmds = [];
      let attrSubCmd = {};
      let tsSubCmd = {};

      attrSubCmd.cmdId = item.index;
      attrSubCmd.entityId = item.id.id;
      attrSubCmd.entityType = item.id.entityType;
      attrSubCmd.scope = "SERVER_SCOPE";
      attrSubCmds.push(attrSubCmd);
      msg.attrSubCmds = attrSubCmds;

      tsSubCmd.cmdId = item.index << 1;
      tsSubCmd.entityId = item.id.id;
      tsSubCmd.entityType = item.id.entityType;
      tsSubCmd.scope = "LATEST_TELEMETRY";
      tsSubCmds.push(tsSubCmd);
      msg.tsSubCmds = tsSubCmds;

      console.log('WebSocket发送给服务端的消息! msg = ' + JSON.stringify(msg));
      webScoket.send({
        data: JSON.stringify(msg)
      })

    } else {
      console.error('WebSocket已关闭! 不能发送消息');
    }
  },

  closeWebSocket() {
    if (this.data.socketOpen && webScoket) {
      webScoket.close(function (res) {
        console.log('WebSocket已关闭！')
      })
    }
  },

  getModelFromDevice(workMode, callback) {
    if (!workMode || workMode == 0) {
      return;
    }

    let deviceId = this.data.deviceInfo.id.id;
    let params = {};
    let subParams = {};
    subParams.workMode = workMode;
    params.method = "GET_PROCESS";
    params.params = subParams;
    params.timeout = 100000;
    
    api.getDeviceModel(deviceId, params).then(res => {
      if (res && res.statusCode == 200) {
        console.log(res);
        let steps = res.data.steps;
        if (steps) {
          steps.forEach(item => {
            item.stepName = (item.funName || item.funName === 0) && this.data.funName[item.funName];
            item.confirmText = item.confirm == 1 ? '是' : '否';
            item.fixTempetureText = item.fixTempeture == 1 ? '是' : '否';
            item.powerText = (item.power || item.power === 0) && this.data.fire[item.power].name;
            item.remindText = item.remind == 1 ? '是' : '否';
            item.stirText = (item.stir || item.stir === 0) && this.data.stir[item.stir].name;
          });
        }

        if (callback) {
          callback(1, steps);
        } else {
          // 设置工序
          this.setData({
            steps: steps
          });

          // 设置当前工序信息
          let wsResult = this.data.wsResult1;
          if (this.data.steps.length > wsResult.currentStepValue && this.data.steps.length >= 1) {
            let currentStepIndex = parseInt(parseInt(wsResult.currentStepValue) - 1);
            if(currentStepIndex >= 0){
              wsResult.currentStepName = this.data.steps[currentStepIndex].stepName;
            }

            this.setData({
              wsResult1: wsResult
            });
          }
        }

      } else {
        callback && callback(0, null);
        console.error(res);
      }
    });
  }, 


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let deviceInfo = JSON.parse(options.deviceInfo);
    deviceInfo.formatProductionDate = deviceInfo.productionDate ? util.formatDate(deviceInfo.productionDate) : null;
    console.log(deviceInfo);
    this.setData({
      title: deviceInfo.type + '(' + (deviceInfo.label || deviceInfo.batchNum || deviceInfo.name) + ')',
      deviceInfo: deviceInfo
    });
    this.connectWebSocket();
  },

  swiperChange(e){
  
   
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    // this.bb();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.setKeepScreenOn({
      keepScreenOn: true,
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    this.closeWebSocket();
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }

  
})