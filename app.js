//app.js
import jwt_decode from 'jwt-decode';

App({
  onLaunch: function () {
    // 展示本地存储能力
    // var logs = wx.getStorageSync('logs') || []
    // logs.unshift(Date.now())
    // wx.setStorageSync('logs', logs)

    this.wxLogin();
    this.userInfo();
  },

  // wx登录
  wxLogin: function (success, fail) {
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        if (res.code) {
          console.log('微信平台登录成功! code = ' + res.code);
          this.saveWxLoginCode(res.code);
          success && success();
        } else {
          console.log('微信平台登录失败！' + res.errMsg);
          fail && fail();
        }
      },
      fail: res => {
        console.log('微信平台登录失败！');
        fail && fail();
      }
    })
  },

  // 获取用户信息
  userInfo: function () {
    wx.getSetting({
      success: res => {
        console.log(res);
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              console.log(res);
              // 可以将 res 发送给后台解码出 unionId
              this.saveUserInfo(res.userInfo);

              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
    })
  },

  onShow: function (data) {
    // 配网小程序,设备配网成功回调
    if (data['scene'] == 1038 && data['referrerInfo'] && data['referrerInfo']['appId'] === 'wx31b43acf6e51cff0') {
      if (this.deviceConnectedCallback) {
        this.deviceConnectedCallback(data);
      }
    } 
    // else {
    //   wx.showToast({
    //     title: '配网失败',
    //     icon: 'success',
    //     duration: 2000
    //   })
    // }
  },

  // 设备配网成功回掉
  deviceConnectedCallback: null,

  // 全局数据
  globalData: {
    wxLoginCode: null,
    userInfo: null,
    webUserInfo: null,
    storeInfo: null,
    token: null,
    refreshToken: null
  },

  clearWebData: function() {
    this.globalData.webUserInfo = null;
    this.globalData.storeInfo = null;
  },

  saveWxLoginCode: function (wxLoginCode) {
    this.globalData.wxLoginCode = wxLoginCode;
    wx.setStorageSync('wxLoginCode', wxLoginCode);
  },

  getWxLoginCode: function () {
    this.globalData.wxLoginCode = this.globalData.wxLoginCode || wx.getStorageSync('wxLoginCode');
    return this.globalData.wxLoginCode;
  },

  saveUserInfo: function (userInfo) {
    this.globalData.userInfo = userInfo;
    wx.setStorageSync('userInfo', userInfo);
  },

  getUserInfo: function () {
    this.globalData.userInfo = this.globalData.userInfo || wx.getStorageSync('userInfo');
    return this.globalData.userInfo;
  },

  saveWebUserInfo: function (webUserInfo) {
    webUserInfo.isManager = webUserInfo.scope == 'CUSTOMER_SUPERVISOR';
    this.globalData.webUserInfo = webUserInfo;
    wx.setStorageSync('webUserInfo', webUserInfo);
  },

  getWebUserInfo: function () {
    this.globalData.webUserInfo = this.globalData.webUserInfo || wx.getStorageSync('webUserInfo');
    return this.globalData.webUserInfo;
  },

  saveStoreInfo: function (storeInfo) {
    this.globalData.storeInfo = storeInfo;
    wx.setStorageSync('storeInfo', storeInfo);
  },

  getStoreInfo: function () {
    this.globalData.storeInfo = this.globalData.storeInfo || wx.getStorageSync('storeInfo');
    return this.globalData.storeInfo;
  },

  saveToken: function (token) {
    this.globalData.token = token;
    wx.setStorageSync('token', token);

    if (token) {
      // 解析token,获取信息
      let decodedToken = jwt_decode(token);
      // 用户信息、token失效时间
      console.log('decodedToken = ' + JSON.stringify(decodedToken));
      let webUserInfo = {};
      webUserInfo.customerId = decodedToken.customerId; // 客户ID: 奶茶机生产商的客户
      webUserInfo.tenantId = decodedToken.tenantId; // 租户ID: 指奶茶机生产商
      webUserInfo.userId = decodedToken.userId; // 用户ID: 具体哪个用户
      webUserInfo.firstName = decodedToken.firstName;
      // webUserInfo.email = decodedToken.sub; // 有乱序字符串,先注释掉
      webUserInfo.scope = decodedToken.scopes && decodedToken.scopes[0];
      this.saveWebUserInfo(webUserInfo);
    }
  },

  getToken: function () {
    this.globalData.token = this.globalData.token || wx.getStorageSync('token');
    return this.globalData.token;
  },

  saveRefreshToken: function (refreshToken) {
    this.globalData.refreshToken = refreshToken;
    wx.setStorageSync('refreshToken', refreshToken);
  },

  getRefreshToken: function () {
    this.globalData.refreshToken = this.globalData.refreshToken || wx.getStorageSync('refreshToken');
    return this.globalData.refreshToken;
  },

  isLogin: function () {
    return this.getToken() ? true : false;
  }
})